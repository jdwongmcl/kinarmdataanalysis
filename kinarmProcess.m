classdef kinarmProcess < handle
  properties (Access = public)
  end
  methods
    function obj=kinarmProcess()
    end
  end

  methods(Static)            % ------STATIC------ %

    % This file is meant to be a single file for students to use to process data.
    % It includes basic functions to:
    % - slice up movements based on speed
    % - create tables of basic variables
    % - do inverse dynamics on stacked data
    % - compute inverse dynamics-> torques, mechanical powers
    % - compute force rate costs
    % 
    % It looks scary, because there are many, many lines. 
    % BUT:
    %
    % It is grouped into one file so that one does not have to flip
    % through a directory, trying to understand what does what.
    % It interfaces with the KINARM matlab repository MATLAB_kinarm. You
    % will need to have this directory in your path for this to work. that
    % directory contains functions like "zip_load()".
    %
    % there are two basic kinds of functions: 
    % 1) accessor functions using 
    % 2) 'pure' functions.
    % accessor functions are meant to be easy to use, and take the
    % rather large c3d structure as input
    % pure functions are meant to clarify the involved computation, so
    % their inputs are the specific data columns needed for computation
    % only.
    % movStack,iIndividualMovements = stackedKinematicsAndTorques(kinC3D,igrp,tDelta,RLBOTH,DO_FIG1))
    %%% INPUTS:
    %%% kinC3D: a kinC3D structure, (via zip_load() and sort_trials());
    %%% %note: given
    %%% data = zip_load(fname);
    %%% data=sort_trials(data,'execution');
    %%% %then you'll want to do
    %%% contData = kinarmProcess.stackedKinematicsAndTorques(data.c3d,igrp);
    %%% optional:
    %%% tDelta (default 0.003, as jer figured out is the case)
    %%% RLBOTH (default = BOTH)
    %%% DO_FIG1 (default = 0)
    %%% OUTPUTS:
    %%% movStack: the single structure containing the stacked fields.
    %%% iIndividualMovements: the places where each of the individual
    %%% movements end.

    % function returnStackedMovements(kinC3D,subStruct,igrp,tDelta)
    %%% as above, but you need to pass in the mass of the subject first.

    % data = loadZipSortSave(fname)
    %%% loads and safes a zip file $fname to a .mat file,
    % for faster analysis later (zip_load is slow!!),
    % and returns the sorted data.

    % lowpassSimple(data, sr, cutoff)
    %%% returns a lowpassed signal based on a sampling rate and cutoff frequency.


    % out = handSpeedPlot(x,y,sr)
    %%% INPUTS
    %%% returns tangential hand velocity in a struct:
    %%% out.vx
    %%% out

    % invDynKinarmVirtualPower() and invDynKinarmJointVirtualPower()
    %%% computes torques and powers for both arms given c3d data (usually
    %%% from stackedKinematicsAndTorques(), using virtual power derivation
    %%% (at bottom of this class doc).

    % positivePower()
    % given mechanicalpower such as data.Right_Power, compute the positive
    % power, and the average positive power.

    % all_from_field

    %%%%%%%%%%%%%%%% less used
    % naturalFrequency()
    % inverseDynamicsKinarm()
    %%% calls the inverseDynamics as computed via the Kinarm proprietary code.
    %%% returns modified C3D structure with torques added to the structure.

    function setpath()
      [filepath,~,~] = fileparts(mfilename('fullpath'));
      addpath(fullfile(filepath,'MATLAB_kinarm'));
      addpath(fullfile(filepath,'example_data/'));
    end

    %% 
    function [movs,movtab]=demo(fname)
    % function movs=demo(fname)
    % demonstrate the functions contained within this file!
    %
      if nargin ==0
        fname = 'example_data.zip';
      end
      kinarmProcess.setpath();
      temp = zip_load(fname);
      data = sort_trials(temp,'execution');
      c3d = data.c3d;

      % each subject needs a mass so that we can estimate the arm's inertia for doing inverse dynamics.
      subj = struct;
      subj.mass = 90;
      % get a continuous stack of movements.
      % for cyclic movements, this function just stacks a list of c3d-files and
      % returns a single c3d structure with the relevant kinematics and kinetic
      % fields stacked one-on-top-of-the-other.
      [movs,movtab] = kinarmProcess.stackedKinematicsAndTorques(c3d,1:length(c3d));
      % get hand speeds.

      vel = kinarmProcess.handSpeedPlot(movs.Right_HandX,movs.Right_HandY,1000);
      tanvel = vel.tanvel;
      [~,indPeaks] = kinarmProcess.getPeaksBetweenAThresholdValue(tanvel,0.3);
      leftRightVel = vel.vx;
      indFullCycles = kinarmProcess.getFullCycleMovements(leftRightVel,indPeaks);
      indArray = kinarmProcess.peakToPeakVelIndices(indPeaks,indFullCycles);

      % performdo inverse dynamics.
      invdyn = kinarmProcess.invDynFromC3D(movs,subj.mass,'addedmass',0.0);

      %
      for i =1:length(indArray)
        curInd = indArray{i};
        plot(movs.Right_L1Vel(curInd));hold on;
      end
    end

    function data = loadZipSortSave(fname,varargin)
      %     function loadZipSortSave(fname,varargin)
      % loads the .zip file, sorts it, and saves the data in a .mat file for
      % future use.
      % by default: 'sort': sorts by execution (could also be 'tp' or 'custom'; see matlab_KINARM/sort_trials())
      %             'savedir' saves to current directory
      %             'savename' appends 'Sorted' to the filename for saving,

      fnameSave = [fname,'Sorted'];
      dirSave = pwd();
      sortMethod = 'execution';
      for i = 1 : 2 : length(varargin)
        option = varargin{i};
        value = varargin{i + 1};
        switch option
          case 'savename'
            fnameSave = value;
          case 'savedir'
            dirSave = value;
          case 'sort'
            sortMethod= value;
        end
      end

      data = zip_load(fname);
      data = sort_trials(data,sortMethod);

      save(fullfile(dirSave,[fnameSave,'.mat']),'data','fname');
    end

    function out = ternary(ifthen,this,that)
      if ifthen
        out=this;
      else
        out = that;
      end
    end
      
    function [movStack,movTable] = stackedKinematicsAndTorques(kinC3D,igrp,varargin)
      % function [movStack,iIndividualMovements]=stackedKinematicsAndTorques(kinC3D,igrp,varargin)
      % input kin is a structure array (i.e. kin(1), kin(2), etc are individual movements)
      % returns the list of movements stacked one-on-top-of-the-other.
      % This function is primarily meant to create the kinematics for
      % analysis of mechanical power, i.e.
      % continuousMovements = stackedKinematicsAndTorques(data)
      % invDynFromC3D(continuousMovements)
      % OPTIONAL:
      % tdelta [0.003]:
      % emgchannels[]: (a cell array of EMGs like
      % {'ACH0','ACH1'})
      % verbose[0]: (0 or 1) for a figure.

      %%% processing optional args
      % defaults
      RLBOTH = 'both';
      EMGCHANNELS = [];
      verbose     = 0;
      tDelta      = 0.003;
      sr          = 1000;
      for i = 1 : 2 : length(varargin)
        option = varargin{i};
        value = varargin{i + 1};
        switch option
          case 'emgchannels'
            EMGCHANNELS = value;
          case 'tdelta'
            tDelta = value;
          case 'verbose'
            verbose = value;
          case 'sr'
            sr = value;

        end
      end
      %%% done processing
      if ~iscell(RLBOTH)
        switch RLBOTH
          case 'right'
            fields = {'Right_M1TorCMD',    'Right_M2TorCMD', ...
              'Right_RecTorque_M1', 'Right_RecTorque_M2','Right_L1Ang',...
              'Right_L2Ang',        'Right_HandX',       'Right_HandY'...
              'Right_L2Vel','Right_L1Vel','Right_L1Acc','Right_L2Acc'};
          case 'left'
            fields = {'Left_M1TorCMD',    'Left_M2TorCMD', 'Left_RecTorque_M1',...
              'Left_RecTorque_M2','Left_L1Ang',    'Left_L2Ang', ...
              'Left_HandX','Left_HandY','Left_L1Vel','Left_L2Vel',...
              'Left_L1Acc','Left_L2Acc'};
          case 'both'
            fields={'Left_M1TorCMD','Left_M2TorCMD',            'Left_RecTorque_M1',...
              'Left_RecTorque_M2',  'Right_M1TorCMD',    'Right_M2TorCMD', ...
              'Right_RecTorque_M1', 'Right_RecTorque_M2','Left_L1Ang',...
              'Left_L2Ang',         'Right_L1Ang',       'Right_L2Ang',...
              'Right_HandX',        'Right_HandY',       'Left_HandX',...
              'Left_HandY','Right_L2Vel','Right_L1Vel','Left_L2Vel','Left_L1Vel',...
              'Right_L2Acc','Right_L1Acc','Left_L2Acc','Left_L1Acc'};
        end
      else
        fields = RLBOTH;
      end

      % add emg channnels to the fields
      oldL = length(fields);
      for i =1:length(EMGCHANNELS)
        fields{oldL+i} = EMGCHANNELS{i};
      end

      %initialize the movement stack.
      movStack = kinC3D(igrp(1));
      tAll =.000:0.001:0.001*(length(movStack.Right_HandX)-1);
      tAll = tAll(:);
      tOff = tAll(end)+tDelta;
      lOff = length(tAll);
      tIndividualMovements(1) = tOff;
      lenIndividualMovements(1) = length(tAll)+round(tDelta*sr)-1;
      % make a movement index that is the entire length of the stacked
      % trial.
      iMov = repmat(igrp(1),lenIndividualMovements(1),1);
      movStack.ithMovement = iMov;

      % now for all other movements, stack the fields on top of each other.
      for i =2:length(igrp)
        kinTemp = kinC3D(igrp(i));

        for jField = 1:length(fields)
          movStack.(fields{jField}) = [movStack.(fields{jField});kinTemp.(fields{jField})];
        end
        % add a time vector and a movement counter.
        tCurMov = 0.000:0.001:0.001*(length(kinTemp.Right_HandX)-1);
        iMov = repmat(igrp(i),length(tCurMov) + round(tDelta*sr)-1, 1); %longer than tCurMov by 2 to account for interpolation. will be bad at the end.
        movStack.ithMovement = [movStack.ithMovement; iMov];

        % concatenate tCurMov to tAll.
        tAll                      = [tAll;tCurMov(:)+tOff];
        tOff                      = tAll(end) + tDelta; %the next offset time.
        tIndividualMovements(i)   = tOff;
        lOff                      = lOff + length(tCurMov);
        lenIndividualMovements(i) = length(tCurMov)+round(tDelta/sr)-1; %
      end
      movStack.ithMovement  = movStack.ithMovement(1:end-2); %see line 230; the last movement is too long by two, so we shorten by 2. X
      movStack.t            = tAll;
      indIndividualMovements  = cumsum(lenIndividualMovements);

      %interpolate those stupid 3ms.
      tProper = 0:0.001:movStack.t(end);
      for jField=1:length(fields)
        if length(movStack.t)==length(movStack.(fields{jField}))
          temp = interp1(movStack.t,movStack.(fields{jField}),tProper');
          movStack.(fields{jField}) = temp(:);
        else
          fprintf(['skipping ,',fields{jField}]);
        end
      end

      % attach interpolated time tProper as t.
      movStack.t                  = tProper(:);

      % overwrite the last iIndividualMovements, since it doesn't include
      % an interpolated bit at the end.
      indIndividualMovements(end)   = length(movStack.Right_HandX); %don't need added delta for last.

      %%%attach speeds for the right and left hand.
      hvr = kinarmProcess.handSpeedPlot(movStack.Right_HandX,movStack.Right_HandY, sr, 'verbose',0);
      movStack.Right_Hand_XVel    = hvr.vx;
      movStack.Right_Hand_YVel    = hvr.vy;
      movStack.Right_Hand_TanVel  = hvr.tanvel;
      hvl = kinarmProcess.handSpeedPlot(movStack.Left_HandX,movStack.Left_HandY, sr, 'verbose',0);
      movStack.Left_Hand_XVel   = hvl.vx;
      movStack.Left_Hand_YVel   = hvl.vy;
      movStack.Left_Hand_TanVel = hvl.tanvel;

      % remove the shoulder x and y
      right_shoxy = [movStack.CALIBRATION.RIGHT_SHO_X,movStack.CALIBRATION.RIGHT_SHO_Y];
      left_shoxy = [movStack.CALIBRATION.LEFT_SHO_X,movStack.CALIBRATION.LEFT_SHO_Y];
      
      movStack.Right_HandX = movStack.Right_HandX - right_shoxy(1);
      movStack.Right_HandY = movStack.Right_HandY - right_shoxy(2);

      movStack.Left_HandX = movStack.Left_HandX - left_shoxy(1);
      movStack.Left_HandY = movStack.Left_HandY - left_shoxy(2);

      % sanity check verbose: plot the left and right hands. 
      if verbose
        f1 = 999;
        tOffsetS=0;

        figure(f1);hold on;
        plot(movStack.Right_HandX,movStack.Right_HandY);
        set(gca,'ColorOrderIndex',1);
        plot(movStack.Right_HandX,movStack.Right_HandY);
        set(gca,'ColorOrderIndex',1);

        f2=998;
        figure(f2);

        nSamp = length(movStack.Right_HandX);
        tplot = 0.001:0.001:(nSamp*0.001);
        tplot = tplot+tOffsetS;

        subplot(3,1,1);
        plot(tplot,movStack.Right_HandX); hold on;
        set(gca,'ColorOrderIndex',1);
        plot(tplot,movStack.Right_HandY);
        set(gca,'ColorOrderIndex',1);
        plot(tplot,movStack.Left_HandX);
        set(gca,'ColorOrderIndex',1);
        plot(tplot,movStack.Left_HandY);
        set(gca,'ColorOrderIndex',v1);

        subplot(3,1,2);
        pOff = 10;
        hold on;
        tOffsetS = tplot(end);
      end

      istart = 1;
      for i = 1:length(igrp)
        trial(i,1)     = i;
        tp = kinC3D(i).TRIAL.TP;
        trialProtocolNum(i,1) = kinC3D(i).TRIAL.TP; % we need this tp number for all.
        loadnum(i,1)          = kinC3D(i).TP_TABLE.Load(tp);
        tgtnumstartend(i,1)   = kinC3D(i).TP_TABLE.Start_Target(tp);
        tgtnumstartend(i,2)   = kinC3D(i).TP_TABLE.End_Target(tp);
        tgtstartxy(i,1)          = kinC3D(i).TARGET_TABLE.X(tgtnumstartend(i,1))/100;%cm
        tgtstartxy(i,2)          = kinC3D(i).TARGET_TABLE.Y(tgtnumstartend(i,1))/100;%cm
        
        tgtendxy(i,1)            = kinC3D(i).TARGET_TABLE.X(tgtnumstartend(i,2))/100;%cm
        tgtendxy(i,2)            = kinC3D(i).TARGET_TABLE.Y(tgtnumstartend(i,2))/100;%cm
        istartend(i,1)        = istart;
        istartend(i,2)        = indIndividualMovements(i);
        sh_r_xy(i,:) = right_shoxy;
        sh_l_xy(i,:) = left_shoxy;

        if isfield(kinC3D(i).TARGET_TABLE,'Logical_Radius')
          tgttype{i,1} = 'circle';
          tgt0wh(i,1)= kinC3D(i).TARGET_TABLE.Logical_Radius(tgtnumstartend(i,1))/100/2;
          tgt0wh(i,2)= kinC3D(i).TARGET_TABLE.Logical_Radius(tgtnumstartend(i,1))/100/2;

          tgt1wh(i,1)= kinC3D(i).TARGET_TABLE.Logical_Radius(tgtnumstartend(i,2))/100/2;
          tgt1wh(i,2)= kinC3D(i).TARGET_TABLE.Logical_Radius(tgtnumstartend(i,2))/100/2;
        else
          tgttype{i,1} = 'rectangle';
          
          tgt0wh(i,1)= kinC3D(i).TARGET_TABLE.rectangle_width(tgtnumstartend(i,1))/100;
          tgt0wh(i,2)= kinC3D(i).TARGET_TABLE.rectangle_height(tgtnumstartend(i,1))/100;
          
          tgt1wh(i,1)= kinC3D(i).TARGET_TABLE.rectangle_width(tgtnumstartend(i,2))/100;
          tgt1wh(i,2)= kinC3D(i).TARGET_TABLE.rectangle_height(tgtnumstartend(i,2))/100;

        end

        % increment istart
        istart = indIndividualMovements(i)+1;
      end
      
      movTable = table(trial,     trialProtocolNum, sh_r_xy,   sh_l_xy,  ...
                      tgttype,    tgt0wh,           tgt1wh,    tgtstartxy,...
                      tgtendxy,   loadnum,          istartend, tgtnumstartend);
      
      % compute minimum distance
      distance_min = zeros(length(trial),1);
      for i = 1:height(movTable)
        distance_min(i,1) = kinarmProcess.minMovDistanceFromMovtab(movTable(i,:));
      end
      movTable.distance_min = distance_min;
    end

    function [c3dData,meanMs] = invDynFromC3D(c3dData, bodyMass, varargin)
      % function [c3dData] = invDynFromC3D(c3dData,varargin)
      % using virtual power approach to modify the equations of motion of the
      % robot arm to allow for mass added to the hand.
      % this function allows you to pass in a c3dData structure and returns
      % an augmented structure with new fields reflecting torques and power.
      % we also return the 'meanMs' matrix, which is the average mass matrix
      % elements across the movement. For now this seems like the best way of
      % doing 'effective mass' for these mostly-shoulder movements.
      % adds the following fields:
      % c3dData.Right_TorquesSegment
      % c3dData.Right_QDot
      % c3dData.Right_TorquesLocal
      % which has
      % c3dData.Right_Power
      % c3dData.Right_L1AccF
      % c3dData.Right_L2AccF
      % c3dData.RightMs

      %%% processing optional args
      % defaults
      addedMass = 0;
      sr = 1000;
      cutoff = 12;
      l2mass = 0.42;%default forarm+wrist length.
      refframe = 'joint'; %default is 'global'.
      for i = 1 : 2 : length(varargin)
        option = varargin{i};
        value = varargin{i + 1};
        switch option
          case 'addedmass'
            addedMass = value;
          case 'sr'
            sr = value;
          case 'lowpass'
            cutoff = 12;
          case 'l2mass'
            l2mass = value;
          case 'refframe'
            refframe = value;
        end
      end
      %%% done processing

      % get inertial parameters
      robParamsRight = kinarmProcess.getKinarmSegmentParameters(c3dData,'right',bodyMass);
      robParamsRight.mHandMass = addedMass;
      robParamsRight.cHandMass = l2mass;
      % get kinematics and robot torques.
      ang = [c3dData.Right_L1Ang,c3dData.Right_L2Ang];
      vel = [c3dData.Right_L1Vel,c3dData.Right_L2Vel];
      acc = [gradient(vel(:,1),1/sr),gradient(vel(:,2),1/sr)];
      acc = kinarmProcess.lowpassSimple(acc,sr,cutoff);
      robTor = [c3dData.Right_RecTorque_M1,c3dData.Right_RecTorque_M2];
      robTor = kinarmProcess.lowpassSimple(robTor,sr,cutoff); %there could be an issue that these recorded torques are very noisy.

      if isequal(refframe,'segment') || isequal(refframe,'global')
        [tauGlob, tauLoc, power, meanMsRight, Ms]= kinarmProcess.invDynKinarmVirtualPower(ang, vel, acc, robTor,robParamsRight);
        theQDot = vel;
      elseif isequal(refframe,'joint') || isequal(refframe,'local')
        [tauLoc, tauGlob, power, meanMsRight, Ms, theQDot]= kinarmProcess.invDynKinarmJointVirtualPower(ang, vel, acc, robTor,robParamsRight);
      else
        fprintf('Sorry: please provide refframe=joint or refframe=segment to get the appropriate inverse dynamics.\n')
        return
      end

      c3dData.Right_TorquesSegment = tauGlob;
      c3dData.Right_QDot = theQDot;
      c3dData.Right_TorquesLocal = tauLoc;
      c3dData.Right_TorquesJoint = tauLoc;
      c3dData.Right_Power = power;
      c3dData.Right_L1AccF = acc(:,1);
      c3dData.Right_L2AccF = acc(:,2);
      c3dData.RightMs = Ms;
      %%%now do the exact same thing for the left arm.
      robParamsLeft = kinarmProcess.getKinarmSegmentParameters(c3dData,'left',bodyMass);
      robParamsLeft.mHandMass = addedMass;
      robParamsLeft.cHandMass = l2mass;

      ang = [c3dData.Left_L1Ang,c3dData.Left_L2Ang];
      vel = [c3dData.Left_L1Vel,c3dData.Left_L2Vel];
      acc = [gradient(vel(:,1),1/sr),gradient(vel(:,2),1/sr)];
      acc = kinarmProcess.lowpassSimple(acc,sr,cutoff);
      robTor = [c3dData.Left_RecTorque_M1,c3dData.Left_RecTorque_M2];
      robTor = kinarmProcess.lowpassSimple(robTor,sr,cutoff); %there could be an issue that these recorded torques are very noisy.

      if isequal(refframe,'global')
        [tauGlob, tauLoc, power, meanMsLeft,Ms]= kinarmProcess.invDynKinarmVirtualPower(ang, vel, acc, robTor,robParamsLeft);
      else
        [tauLoc, tauGlob, power, meanMsLeft, Ms]= kinarmProcess.invDynKinarmJointVirtualPower(ang, vel, acc, robTor,robParamsLeft);
      end
      c3dData.Left_TorquesSegment = tauGlob;
      c3dData.Left_TorquesLocal = tauLoc;
      c3dData.Left_TorquesJoint = tauLoc;
      c3dData.Left_Power = power;
      c3dData.Left_L1AccF = acc(:,1);
      c3dData.Left_L2AccF = acc(:,2);
      c3dData.LeftMs = Ms;
      meanMs.Left = meanMsLeft;
      meanMs.Right = meanMsRight;
    end

    function [data_s, m, sd] = sphering(data)
      m   = mean(data, "omitnan");
      sd  = std(data, "omitnan");
      data_s = (data - m) ./ sd;
    end

    function [data] = desphering(data_s, m, sd)
      data = data_s*sd + m;
    end

    function [seThresh] = sliceBellShapedVelViaThreshold(c3d,movtab,varargin)
      %function [seThresh] = sliceBellShapedVelViaThreshold(c3d,movTab,varargin)
      % return a list of starts and ends that is the same size as movTab.
      % uses the projected velocity onto the movement axis to slice based on thresholds.

      % default parameters
      tvThresh      = 0.1;%threshold of tanvel, m/s
      dTimeDownSec  = 1.5; %after hitting the target, how long until v~0?
      sr            = 1000;
      verbose       = 0;
      tvThreshLow   = 0.02;
      fignum        = 1000; 
      figsub        = 1;
      for i = 1 : 2 : length(varargin)
        option = varargin{i};
        value = varargin{i + 1};
        switch option
          case 'verbose'
            verbose = value;
          case 'sr'
            sr = value;
          case 'timedown'
            dTimeDownSec = value;
          case 'tvthresh'
            tvThresh = value;
          case 'tvthreshlow'
            tvThreshLow = value;
          case 'fignum'
            fignum = value;
          case 'figsub'
            figsub = value;
        end
      end
      % /default parameters

      % initialize pre-loop
      counter = 1;
      clear distances peakSpeed durations
      failStarts = 0;
      failEnds = 0;
      failFits = 0;
     
      %
      starti = movtab.istartend(:,1);
      seThresh = ones(length(starti),2)*nan;
      for iMov = 2:(length(starti)-1)
        curInd = starti(iMov)+(sr*dTimeDownSec):starti(iMov+1)+(sr*dTimeDownSec);

        vMat = [c3d.Right_Hand_XVel(curInd),c3d.Right_Hand_YVel(curInd)]';
        startxy = movtab.tgtstartxy(iMov,:)';
        endxy   = movtab.tgtendxy(iMov,:)';
        startxy = [c3d.Right_HandX(curInd(1));c3d.Right_HandY(curInd(1))];
        endxy = [c3d.Right_HandX(curInd(end));c3d.Right_HandY(curInd(end))];
        
        r_se    = endxy - startxy;
        r_sen    = r_se./norm(r_se);

        pvCur = dot(repmat(r_sen,1,length(vMat)),vMat);
        tvCur = c3d.Right_Hand_TanVel(curInd);
        if (pvCur(1) > tvThresh)
          fprintf('Warning! Movement %i is not even below %.3f m/s at the beginning.\n', iMov, tvThresh);
          %calculate the speed of the hand with handspeedplot
          continue
        end

        istartHigh_PV  = find(pvCur>tvThresh,1,'first');

        istartPV    = find(pvCur(1:istartHigh_PV)<tvThreshLow,1,'last');
        istartOut   = istartPV + curInd(1)-1;

        figthresh = 100;
        if verbose > 0
          figure(fignum+figthresh+figsub);
          subplot(2,2,1);
          plot(c3d.Right_HandX(curInd),c3d.Right_HandY(curInd));hold on;
          xlabel('Hand x (m)');
          ylabel('Hand y (m)');
          [tgt0,tgt1] = kinarmProcess.xyTargets(movtab(iMov,:));
          plot(tgt0(:,1),tgt0(:,2),'g','linewidth',2);
          plot(tgt1(:,1),tgt1(:,2),'r','linewidth',1.5);
          xlim([-.5,.5]);
          ylim([-.1,.9])

          if verbose > 1
            hold off;
          end

          subplot(2,2,2);
          t = (curInd-curInd(1))*0.001;
          plot(t, pvCur);hold on;
          plot(t, tvCur);
          line(xlim(),[tvThreshLow,tvThreshLow]);
          line(xlim(),[tvThresh,tvThresh]);
          xlabel('Time (s)');
          ylabel('Tangential and movaxis hand speed (m/s)');

          if verbose > 1
            fprintf('Showing movement %i; distance %.2f \n', iMov, norm(r_se));
            hold off;
          end
        end

        % check to see if the movement started below lowcut. 
        if isempty(istartPV)
          fprintf('Movement %i did not start below the low threshold.\n',iMov);
          failStarts = failStarts + 1;

            if verbose>1
              pause;
            end
            continue %failed start below lowcutoff search.
        end
          
        
        iend_High = find(pvCur(istartHigh_PV:end)<tvThreshLow,1,'first');
        if isempty(iend_High)
          fprintf('Movement %i did not end below the low threshold.\n',iMov);
          failEnds = failEnds + 1;
            if verbose>1
              pause;
            end
          
          continue %failed end below lowcutoff search.
        end
        iendPV = istartHigh_PV-1 + iend_High; 
        iendOut = iendPV + curInd(1) -1;

        if verbose
          figure(fignum+figthresh+figsub);
          line([t(istartPV),t(istartPV)],[0,1]);
          line([t(iendPV),t(iendPV)],[0,1]);
          title(sprintf('Movement %i; distance %.2f',iMov,norm(r_se))); 
          ylim([0,1])
          if verbose > 1
            hold off;
            pause();
          end
        end

%         doCriticalDampingFit = 0;
%         if doCriticalDampingFit
%           % attempt a critically-damped fit on both ends.
%           verbose = 1;
%           nsampfit = 100;
%           indForFit = iendPV-nsampfit:iendPV;
%           indForFitafter = iendPV-nsampfit:(iendPV+300);
%           tvForFit = pvCur(indForFit);
%           tvForAfter = pvCur(indForFitafter);
%           tafter = (0:(length(tvForAfter)-1))*1/sr;
%           t = (0:(length(tvForFit)-1))*1/sr;
%           [A, T1, T2, offset, Ssq] = fit_critical_damping( t, tvForFit,[.13,.1,.025,0]);
%           %fitting critically damped oscillations to the equation
%           ttest = (0:300)*1/sr;
% 
%           y_cd = A*(1 + ttest/T2) .* exp( -ttest/T1 ) + offset;
%           iendfit_rel_to_fit = find(y_cd>0.001,1,'last');
%           if isempty(iendfit_rel_to_fit)
%             fprintf('the fit did not cross zero in 300ms.');
%             failFits = failFits +1;
%             continue
%           end
%           iendfit = iendPV+iendfit_rel_to_fit - nsampfit;
%           if verbose
%             figure(9999); hold off;
%             plot(t,tvForFit,'linewidth',3);
%             hold on;
%             plot(tafter,tvForAfter);
%             plot(ttest,y_cd);
%             fprintf('fit result');
%           end
%         end
        
        xystart           = [c3d.Right_HandX(curInd(istartPV)),c3d.Right_HandY(curInd(istartPV))];
        xyend             = [c3d.Right_HandX(curInd(iendPV)),c3d.Right_HandY(curInd(iendPV))];
        seThresh(iMov,:)  = [istartOut,iendOut];
      end

      % finally, plot over the entire set of handmovements the start/end measurements.
      % this final check is necessary to determine that the slice indices are well-aligned. 
      figallcut = 400;
      if verbose > 1 % only plot this entire dataset if we really wanna see it. 
        figure(fignum+figallcut+figsub);
        t = ((1:length(c3d.Right_Hand_TanVel))-1)*0.001;
        plot(t,c3d.Right_Hand_TanVel);
        senotnan = ~isnan(seThresh(:,1));hold on;
        plot(t(seThresh(senotnan,1)),c3d.Right_Hand_TanVel(seThresh(senotnan,1)),'bx')
        plot(t(seThresh(senotnan,2)),c3d.Right_Hand_TanVel(seThresh(senotnan,2)),'rx');
        tempx = xlim();
        line([0,tempx(2)],[tvThreshLow,tvThreshLow])
        
        xlabel('Time (s)');
        ylabel('Hand speed (m/s)')
      end
    end

    function [seProj0] = sliceEndsFromProjectedVZero(c3d, movtab, varargin)
      % function out = projectedVZeroForMovementEnd(c3d, varargin)
      % project the hand's velocity onto the movement axis. take first 0
      % in the projected axis after the movement is done.
      % currently the endpoint of every movement is calculated.
      % reachSpeedAccuracyFromScored() begins at the first Projected-0-crossing, so
      % it skips the first movement (which is usually a weird movement, so
      % it's a good one to skip).

      % default parameters
      verbose = 0;
      for i = 1 : 2 : length(varargin)
        option = varargin{i};
        value = varargin{i + 1};
        switch option
          case 'verbose'
            verbose = value;
        end
      end
      % /default parameters

      % for each movement find [endi]
      endProj0 = zeros(height(movtab),1)*nan;
      for i = 1:height(movtab)
        if i == height(movtab)
          isearch = endProj0;
          endProj0(i) = movtab.istartend(i,2);
        else
          isearch = movtab.istartend(i,2):movtab.istartend(i+1,2);

          startxy = movtab.tgtstartxy(i,:);
          endxy   = movtab.tgtendxy(i,:);
          r_se    = endxy - startxy;
          r_se    = r_se./norm(r_se);

          % project the hand velocity vMat onto the movVec mat.
          vMat        = [c3d.Right_Hand_XVel(isearch)';c3d.Right_Hand_YVel(isearch)'];
          vOnMovVec   = dot(repmat(r_se,1,length(vMat)),vMat);
          vShift      = circshift(vOnMovVec,-1);
          tempi       = find(vOnMovVec > 0 & vShift<=0,1,'first');

          % if we do not have a speed-reversal
          if isempty(tempi)
            fprintf('Did not find a projected speed reversal.\n')
            continue
          else
            endProj0(i) = tempi + isearch(1)-1;
          end

          if verbose == 2
            figure(9981);
            ploti = movtab.istartend(i,1):isearch(end);
            previ = movtab.istartend(i,1):isearch(1);

            % hand space.
            subplot(2,2,1);
            plot(c3d.Right_HandX(ploti),c3d.Right_HandY(ploti)); hold on;
            plot(c3d.Right_HandX(previ),c3d.Right_HandY(previ));
            hxy0 = [c3d.Right_HandX(ploti(1));c3d.Right_HandY(ploti(1))];
            line([hxy0(1), hxy0(1)+r_se(1)], [hxy0(2), hxy0(2)+r_se(2)],'color','k');
            plot(hxy0(1),hxy0(2),'go','markersize',10)
            plot(c3d.Right_HandX(endProj0(i)),c3d.Right_HandY(endProj0(i)),'rx','markersize',10);
            axis([0,.6,0,.6])
            xlabel('X (m)');
            ylabel('Y (m)');
            title(sprintf('number %i\n',i))
            hold off;

            % projected velocity
            subplot(2,2,2);
            vMatP     = [c3d.Right_Hand_XVel(ploti)';c3d.Right_Hand_YVel(ploti)'];
            dotplot   = dot(repmat(r_se,1,length(vMatP)),vMatP);
            plot(dotplot);
            frommovi = endProj0(i)-ploti(1);hold on;
            plot(endProj0(i)-ploti(1),dotplot(frommovi),'rs','markersize',10); hold off;
            xlabel('time from mov begin (ms)');
            ylabel('Vproj (m/s)');
            title('projected v; t(0) = mov start');

            % tangential velocity
            subplot(2,2,3);
            plot(c3d.Right_Hand_TanVel(ploti)); hold on;
            plot(endProj0(i)-ploti(1),c3d.Right_Hand_TanVel(endProj0(i)),'rs','markersize',10); hold off;
            xlabel('time from mov begin (ms)');
            ylabel('tangential velocity (m/s)');

            % the actual projected for data
            subplot(2,2,4);
            plot(vOnMovVec);hold on;
            plot(tempi,vOnMovVec(tempi),'rs'); hold off;
            xlabel('time (ms)');
            ylabel('Vproj(m/s)');
            title('projected v; t(0) = tgt hit');

            fprintf('number %i\n',i)
            pause();
          end % verbose
        end %if end case. else
      end %/height loop

      % make the starts all be end+1. remove the first reach since it's
      % always weird.
      startProj0  = endProj0 + 1;
      startProj0  = startProj0(1:end-1);
      endProj0    = endProj0(2:end);
      seProj0     = [startProj0(:),endProj0(:)];
      % the first movement we ignore.
      seProj0     = [nan,nan;seProj0];

    end

    function [movtab, c3d] = tableAddSpeedAndDistanceFromSliced(c3d, movtab, se, varargin)
      % function out = reachSpeedAccuracyFromScored(c3d, movtab, se, varargin)
      % input: a c3d cell array
      % movtab: the table of starts, ends, etc.
      % c3d = data.c3d
      %
      % outMov:
      %       outMov.distance
      %       outMov.duration
      %       outMov.avgspeed
      %       outMov.peakspeed
      %       outMov.whichTPTarget
      %       outMov.endpointScoredX
      %       outMov.endpointScoredY
      %       outMov.endpointdx
      %       outMov.endpointdy
      %       outMov.startIScore
      %       outMov.endIScore
      %       outMov.endpointScoredX
      %       outMov.endpointScoredY
      %       outMov.velproj_rs
      %       outMov.time_rs
      % consider passing the c3d
      % only 2 figures here, the overhead plot and the tanvel.
      ds          = 20;
      verbose      = 1;
      sr = 1000;
      subplotcell = {1,1,1};
      txt         = '';
      threshZ     = 3;
      colorgain   = 1;
      fignum      = 2000;
      figsub      = 1;
      post_sec    = 0.5;
      for i = 1 : 2 : length(varargin)
        option = varargin{i};
        value = varargin{i + 1};
        switch option
          case 'ds'
            ds = value;
          case 'verbose'
            verbose = value;
          case 'subplot'
            subplotcell = value;
          case 'title'
            txt = value;
          case 'colorgain'
            colorgain = value;
          case 'fignum'
            fignum = value;
          case 'figsub'
            figsub = value;
          case 'sr'
            sr = value;
          case 'post_sec'
            post_sec = value;
        end
      end

      n = length(se); %

      % initialize outputs at nans; they'll get inserted into the table.
      [whichTPtarget, distance,   duration,   avgspeed, ...
       peakspeed,     startiSlice,endiSlice,  r_ang, ...
       mean_pv_post, mean_tv_post ] ...
        = deal(ones(n,1)*nan);

      % initialize nx2 vectors
      endpoint   = [distance,distance]; %vector
      startpoint = [distance,distance]; %vector
      r_mov_n    = [distance,distance]; %vector

      % initialize nxdownsampnum vectors.
      ds = 200; % for tanvels. 
      [time_rs, velproj_rs] = deal(zeros(n,ds));
      hand = zeros(n, ds, 2);

      % loop through all movements
      for ic = 1:n
        inds = (se(ic,1):se(ic,2));
        if isnan(sum(inds)) || isempty(inds) && verbose > 0
          fprintf('Skipping movement %i \n',ic);
        else
          % time, tangential velocity, hand position.
          t   = (inds-inds(1))*0.001;
          hx  = c3d.Right_HandX(inds); hy = c3d.Right_HandY(inds);
          tvp = c3d.Right_Hand_TanVel(inds);

          % label the movement based on the tp at movement start.
          curmovnum         = c3d.ithMovement(inds(1));
          whichTPtarget(ic) = movtab.trialProtocolNum(curmovnum);

          startiSlice(ic)   = inds(1);
          endiSlice(ic)     = inds(end);

          % from start and end points, compute dist, dur, avg speed.
          xystart         = [hx(1),     hy(1)];
          xyend           = [hx(end), hy(end)];
          distance(ic)    = norm(xystart - xyend);
          duration(ic)    = t(end);
          avgspeed(ic)    = distance(ic) / duration(ic);

          % get peak speed
          peakspeed(ic)   = max(tvp);

          % start and end
          startpoint(ic,:)= xystart;
          endpoint(ic,:)  = xyend;

          % get projected velocity info
          r_mov    = xyend - xystart;
          r_mov_n(ic,:)   = (r_mov./norm(r_mov))';
          r_ang(ic)   = atan2(r_mov_n(ic,2),r_mov_n(ic, 1));
          
          % take the tan and proj speed afterwards, for 0.5s (depends on r_mov_n
          samp_post = round(post_sec/(1/sr));
          inds_post = (inds(end)+1):(inds(end) + 1 + samp_post);
          vmat_post = [c3d.Right_Hand_XVel(inds_post),c3d.Right_Hand_YVel(inds_post)]';
          projv_post= dot(repmat(r_mov_n(ic,:)',1,length(inds_post)),vmat_post);
          tanv_post = c3d.Right_Hand_TanVel(inds_post);
          mean_pv_post(ic) = mean(projv_post);
          mean_tv_post(ic) = mean(tanv_post);

          % downsample the handspeed, and save it. 
          iplot = inds(1):inds(end);
          vMat = [c3d.Right_Hand_XVel(iplot),c3d.Right_Hand_YVel(iplot)]';
          velproj = dot(repmat(r_mov_n(ic,:)',1,length(vMat)),vMat);
          tpe         = (iplot-iplot(1))*0.001;
          tvpe        = c3d.Right_Hand_TanVel(iplot);

          Nhalf = round(ds/2);
          Nrest = ds-Nhalf;
          [~,ind] = max(velproj);
          tout = [linspace(t(1),t(ind),Nhalf)';linspace(t(ind+1),t(end), Nrest)'];
          pvout = interp1(t, velproj, tout)';

          time_rs(ic,:)    = tout';
          velproj_rs(ic,:) = pvout';
          hand(ic,:, 1) = (interp1(tpe,vMat(1,:),   tout))';
          hand(ic,:, 2) = (interp1(tpe,vMat(2,:),   tout))';
          
          if verbose
            
            % plot the tangential velocity.
            figtv = 200;
            figure(fignum+figtv+figsub);
            subplot(subplotcell{:});

            plot(tpe(1:ds:end),tvpe(1:ds:end),'color',[0,0,1*colorgain,.2]);
            hold on;
            title(txt);
            xlabel('Time (s)')
            ylabel('tanvel (m/s)')
            xlim([0,1.5]);
            ylim([0,2]);
            title('speedAndDuration: speed')

            figxy = 100;
            figure(fignum+figxy+figsub);
            %set(gcf,'renderer','painters');
            subplot(subplotcell{:});
            plot(hx(1:ds:end),hy(1:ds:end),'color',[0,0,1,.2]);
            hold on;
            % plot the start point info.
            plot(startpoint(ic,1),startpoint(ic,2),'.','markersize',...
              10,'color',[0,1,0,.1]);
            % plot the endpoint info.
            plot(endpoint(ic,1),endpoint(ic,2),'s','markersize',...
              5,'color',[1,0,0,.1]);
            title(txt);
            xlim([-.2,.6]);
            ylim([-.2,.6]);
            xlabel('X (m)');
            ylabel('Y (m)');
            title('speedAndDuration: hand')

          end
        end % end doPlot
      end % end loop through all half-cycles.

      % return by-movement metrics: distance, duration, speed, true endpoints
      movtab.distance        = distance;
      movtab.duration        = duration;
      movtab.avgspeed        = avgspeed;
      movtab.peakspeed       = peakspeed;
      movtab.r_ang           = r_ang;
      movtab.whichTPtarget   = whichTPtarget;
      movtab.endpointScoredXY = endpoint;
      % for simplicity, do some subtraction in the table.
      movtab.endpointdXY      = movtab.endpointScoredXY - movtab.tgtendxy;
      movtab.startiSlice     = startiSlice;
      movtab.endiSlice       = endiSlice;
      movtab.startpointScoredXY = startpoint;
      movtab.time_rs         = time_rs;
      movtab.velproj_rs      = velproj_rs;
      movtab.hand            = hand;
      movtab.mean_pv_post    = mean_pv_post;
      movtab.mean_tv_post    = mean_tv_post;
      
    end

    function [tgtTable,movTab] = tableTgtFromMovTab(movTab,varargin)
      % function [movTab,outTgt] = tableTgtFromMovTab(movTab,varargin)
      % return target-specific information: 

      conf    = 0.95;
      threshZ = 5;
      fignum  = 3000;
      figsub  = 1;
      verbose = 1;
      for i = 1 : 2 : length(varargin)
        option = varargin{i};
        value = varargin{i + 1};
        switch option
          case 'verbose'
            verbose = value;
          case 'conf'
            conf = value;
          case 'threshz'
            threshZ = value;
          case 'figsub'
            figsub = value;
        end
      end

      tgtnumber = unique(movTab.whichTPtarget);
      tgtnumber = tgtnumber(~isnan(tgtnumber));%clean nan.
      
      %%% initialize output table. it's big.
      tgtTable = table('Size',[1,14],'VariableTypes', ...
        {'double','double','double','double', ...
         'double','double','double','double', ...
         'double','double','double','double', ...
         'double','double'});

      tgtTable.Properties.VariableNames={
        'tgtnumber', 'cmddistance'  ,'perfdistance', 'peakspeed', ...
         'duration', 'count'        , 'nTgt'       , 'perctgt', ...
         'area'    , 'angleElps2mov', 'errorMajor' , 'errorMinor', ...
         'errorMovementAxis', 'errorPerpendicularAxis'};

      %%% loop
      n_rs = size(movTab.time_rs,2);
      n_tgt= length(tgtnumber); 
      [mtimes,mvelproj,stdvelproj] = deal(zeros(n_tgt,n_rs));
      
      for it = 1:length(tgtnumber)
        % get the target num. get the indices for current target,
        isCurTgt = movTab.whichTPtarget == tgtnumber(it);
        nTgt       = sum(isCurTgt);

        % get all of the projected velocities.
        times   = movTab.time_rs(isCurTgt,:);
        velproj = movTab.velproj_rs(isCurTgt,:);
        mtimes(it,:)    = mean(times,1,"omitnan");
        mvelproj(it,:)  = mean(velproj,1,"omitnan");
        stdvelproj(it,:)= std(velproj,1,"omitnan");

        % r_endpoints is the set of all endpoint column vectors.
        r_endpoints   = movTab.endpointScoredXY';
        r_startpoints = movTab.startpointScoredXY';
        
        % r_mov is the movement column vector.
        indTgt      = find(movTab.trialProtocolNum == tgtnumber(it)); indTgt =indTgt(1);
        startxy     = movTab.tgtstartxy(indTgt,:)';
        endxy       = movTab.tgtendxy(indTgt,:)';
        r_mov       = endxy - startxy;
        cmddistance = kinarmProcess.minMovDistanceFromMovtab(movTab(indTgt,:));
        perfdistance   = mean(movTab.distance(isCurTgt),'omitnan');
        peakspeed   = mean(movTab.peakspeed(isCurTgt),'omitnan');
        duration    = mean(movTab.duration(isCurTgt),'omitnan');
        r_mov       = r_mov ./ norm(r_mov);

        % robustZDist column, abs difference from median.
        movTab.robustZDist(isCurTgt) = kinarmProcess.robustZScore(movTab.distance(isCurTgt));

        % robustZDur column, abs distance from median.
        movTab.robustZDur(isCurTgt)  = kinarmProcess.robustZScore(movTab.duration(isCurTgt));

        % iR is the set of robust movements iR, as iT & robust(DIST).
        iR = isCurTgt & movTab.robustZDist<threshZ;% & movTab.robustZDur<threshZ;

        % plot the endpoints that are going to included in accuracy
        % measures.
        if verbose
          figure(fignum+figsub);
          scatter(r_endpoints(1,iR),r_endpoints(2,iR),...
            10,repmat(cmddistance,1,sum(iR)),'filled'); hold on;
        end
        % of iR movements, compute covariance matrix,find eigenvals,calc
        % area and axis lengths
        covT  = cov(r_endpoints(:,iR)');

        % only if we have enough points do we compute the rest. 
        if sum(iR) <3
          fprintf('Warning: less than 3 reaches are robust. \n');
          fprintf('Skipping ellipse calculation.\n')
          
          count                   = sum(iR);
          area                    = nan;
          angle                   = nan;
          errorMajor              = nan;
          errorMinor              = nan;
          errorMovementAxis       = nan;
          errorPerpendicularAxis  = nan;
          % r_mov is already computed
        
          meanxytend                   = zeros(2,1)*nan;
          % cov is already computed, but can be a scalar:
          if length(covT) == 1
            covT = {covT};
          end
        else
          count = sum(iR);
          [eigvec,eigval] = eig(covT);

          k               = 2.447; %95% conf
          m2cm            = 100;
          area            = prod(diag(sqrt(eigval))*k*m2cm)*pi/4;

          % plot the covariance ellipse
          meanxytend           = mean(r_endpoints(:,iR),2);
          [dT,ptr]        = error_ellipse(covT,meanxytend,'conf', conf, 'verbose', verbose);  %another plot
          

          if verbose
            plotvals        = {'linewidth',2};
            [xytgt0,xytgt1] = kinarmProcess.xyTargets(movTab(indTgt,:));
            plot(xytgt0(:,1),xytgt0(:,2),'k','linewidth',1);
            plot(xytgt1(:,1),xytgt1(:,2),'k','linewidth',1);

            set(ptr,plotvals{:});
            ds = 50; %consider overplotting. 
            repdist = repmat(cmddistance,length(dT),1);
            %s = scatter(dT(1:ds:end,1),dT(1:ds:end,2),20,repdist(1:ds:end),"filled");%overplot
            s.AlphaData = 0.2;
            axis equal;
          end

          % get the indices for major and minor axis.
          elpsMat     = dT - mean(dT); %ellipse centred at 0.
          normElps    = vecnorm(elpsMat,2,2); % 2 norm, vectors are along dimension 2. 
          [~, imajor] = max(normElps);
          [~, iminor] = min(normElps);          
          
          % circlerowmat is a row-aligned circular version of the ellipse.
          % this is useful for getting the ellipse direction corresponding
          % to movement.
          circlerowmat= [elpsMat(:,1)./normElps,elpsMat(:,2)./normElps];

          % using major axis location, get the angle of the ellipse.
          r_elps      = circlerowmat(imajor,:)';
          angle           = acos(dot(r_elps, r_mov)); %angle of ellipse
          

          % get the indices for movementaxis and perpendicular axis.
          [~, imovax] = max(abs(dot(repmat(r_mov,1,length(elpsMat)),circlerowmat')));
          [~, iprpax] = min(abs(dot(repmat(r_mov,1,length(elpsMat)),circlerowmat')));


          % draw the results
          if verbose 
            line([meanxytend(1), dT(imovax,1)], [meanxytend(2), dT(imovax,2)],'color','r');
            line([meanxytend(1), dT(iprpax,1)], [meanxytend(2), dT(iprpax,2)],'color','g');
          end
          % errors 
          errorMajor              = norm(elpsMat(imajor,:));
          errorMinor              = norm(elpsMat(iminor,:));
          errorMovementAxis       = norm(elpsMat(imovax,:));
          errorPerpendicularAxis  = norm(elpsMat(iprpax,:));


        end
        nTtl    = height(movTab);
        perctgt = count/nTtl;

        xymeanstarts(it,:) = mean(r_startpoints(:,isCurTgt),2,'omitnan')';
        xymeanends(it,:)   = mean(r_endpoints(:,isCurTgt),2,'omitnan')';
        
        % things that get added later.
        r_mov_all(it,:)       = r_mov(:)';
        r_ang_all(it,:)       = atan2(r_mov(2),r_mov(1));
        meanxytend_all(it,:)  = meanxytend(:)';
        covT_all(it,:)        = covT(:)';

        tgtTable(it,:) = { tgtnumber(it),...
                           cmddistance,... 
                           perfdistance, ...
                           peakspeed, ...
                           duration,...
                           count, ...
                           nTgt, ...
                           perctgt, ...
                           area, ...
                           angle, ...
                           errorMajor, ...
                           errorMinor, ...
                           errorMovementAxis, ...
                           errorPerpendicularAxis};
      end
      
      if verbose

        % set the range of the colormap
        colormap("parula");
        colorbar();
        b = ver();
        axis([-.3,.4,-.1,.6]);
        title(sprintf('figure: %i target variability ellipses',fignum+figsub));
        xlabel('hand x (m)');
        ylabel('hand y (m)');

        if str2double(b(1).Version) < 10.12
          caxis([0,.7]);
        else
          clim([0,0.7]);
        end
      end

      % add the meanstarts and ends as columns.
      tgtTable.r_mov        = r_mov_all;
      tgtTable.r_ang        = r_ang_all;
      tgtTable.meanxytend   = meanxytend_all;
      tgtTable.covT         = covT_all;
      tgtTable.xymeanstart  = xymeanstarts;
      tgtTable.xymeanend    = xymeanends;
      tgtTable.mtimes       = mtimes;
      tgtTable.mvelproj     = mvelproj;
      tgtTable.stdvelproj   = stdvelproj;

    end % targetAccuracyFromMovTab

    function [dist,xyplot] = minMovDistanceFromMovtab(mtl)
      [dist, xyplot] = kinarmProcess.minMovDistance2(...
        mtl.tgtstartxy,mtl.tgtendxy,mtl.tgttype,...
        mtl.tgt0wh,mtl.tgt1wh);
      
    end

    function [xy0, xy1] = xyTargets(mtl)
      switch mtl.tgttype{:}
        case 'circle'
          theta = (0:0.01:2*pi)';
          r   = mtl.tgt0wh(1);%tgtwidth and height the same.
          xy0 = repmat(mtl.tgtstartxy,length(theta),1) + [r*cos(theta),r*sin(theta)];

          r   = mtl.tgt1wh(1);%tgtwidth and height the same.
          xy1 = repmat(mtl.tgtendxy,length(theta),1) + [r*cos(theta),r*sin(theta)];

        case 'rectangle'
          half0 = mtl.tgt0wh./2;
          half1 = mtl.tgt1wh./2;

          dcorners0 =[half0(1),-half0(2);
                      half0(1), half0(2);
                     -half0(1), half0(2);
                     -half0(1),-half0(2);
                      half0(1),-half0(2);];
          

          dcorners1 =[half1(1),-half1(2);
                      half1(1), half1(2);
                     -half1(1), half1(2);
                     -half1(1),-half1(2);
                      half1(1),-half1(2)];


          xy0 = mtl.tgtstartxy + dcorners0; 

          xy1 = mtl.tgtendxy + dcorners1;
        end
      
    end

    function [dist, dxy0, dxy1] = minMovDistance(...
        startxy, endxy, tgttype, ...
        tgt0wh, tgt1wh)
      x0    = startxy(1);
      x1    = endxy(1);
      y0    = startxy(2);
      y1    = endxy(2);

      xy0   = [x0;y0];
      xy1   = [x1;y1];

      r_01 = [x1-x0;y1-y0];
      r_01 = r_01/norm(r_01);
      
      if strcmp(tgttype,'circle')
        
        r_circ = [tgtwidth;tgtwidth];%it's a circle.
        dxy0 = r_01 * dot(r_01,r_circ);
        dxy1 = r_01 * (-dot(r_01,r_circ));

      elseif strcmp(tgttype,'rectangle')
        if dot(r_01, [0;1]) == 1 %upward reach. 
          dxy0 = [0;tgt0wh(2)/2];
          dxy1 = [0;0];
          
        elseif dot(r_01, [0 ;-1]) == 1 %downward reach.
          dxy0 = [0;0]; 
          dxy1 = [0;tgt1wh(2)/2];  

        elseif dot(r_01, [ 1, 0]) == 1 %rightward reach.
          dxy0 = [tgt0wh(1)/2;0]; 
          dxy1 = [0;0];  

        elseif dot(r_01, [-1; 0]) == 1 %leftward reach.  
          dxy0 = [0;0]; 
          dxy1 = [tgt1wh(1)/2;0];
        end
      end
      dist = norm(xy1+dxy1-(xy0+dxy0));
    end

    function [dist, dxy0, dxy1] = minMovDistance2(...
        startxy, endxy, tgttype, ...
        tgt0wh, tgt1wh)
      x0    = startxy(1);
      x1    = endxy(1);
      y0    = startxy(2);
      y1    = endxy(2);

      xy0   = [x0;y0];
      xy1   = [x1;y1];

      r_01 = [x1-x0;y1-y0];
      r_01 = r_01/norm(r_01);
      
      if strcmp(tgttype,'circle')
        
        r_circ0 = [tgt0wh(1);tgt0wh(1)];%it's a circle.
        r_circ1 = [tgt1wh(1);tgt1wh(1)];%it's a circle.
        dxy0 = r_01 * dot(r_01,r_circ0);
        dxy1 = r_01 * (-dot(r_01,r_circ1));

      elseif strcmp(tgttype,'rectangle')
        if dot(r_01, [0;1]) == 1 %upward reach. 
          dxy0 = [0; tgt0wh(2)/2];
          dxy1 = [0;-tgt1wh(2)/2];
          
        elseif dot(r_01, [0 ;-1]) == 1 %downward reach.
          dxy0 = [0;-tgt0wh(2)/2]; 
          dxy1 = [0; tgt1wh(2)/2];  

        elseif dot(r_01, [ 1, 0]) == 1 %rightward reach.
          dxy0 = [ tgt0wh(1)/2;0]; 
          dxy1 = [-tgt1wh(1)/2;0];  

        elseif dot(r_01, [-1; 0]) == 1 %leftward reach.  
          dxy0 = [-tgt0wh(1)/2;0]; 
          dxy1 = [ tgt1wh(1)/2;0];
        end
      end
      dist = norm(xy1+dxy1-(xy0+dxy0));
    end

    %function finTab = reprocessTargetAccuracyForLength(movTab,varargin)
    function distTab = subjectAccuracyForGivenLength(movTab,varargin)

      ampDesM = .1;
      ampThreshM = 0.02;
      verbose = 0;
      cmdaccuracyM = 0.025;
      whichdist = 'perfdistance';
      for i = 1 : 2 : length(varargin)
        option = varargin{i};
        value = varargin{i + 1};
        switch option
          case 'ampdesm'
            ampDesM = value;
          case 'ampthreshm'
            ampThreshM = value;
          case 'verbose'
            verbose = value;
          case 'cmdaccuracym'
            cmdaccuracyM = value;
          case 'perf1cmd2'
            if value == 1
              whichdist = 'perfdistance';
            else
              whichdist = 'cmddistance';
            end
        end
      end

      distTab = table('Size',[1,7],'VariableTypes',{'double','double','double','double','double','double','double'});
      distTab.Properties.VariableNames = {'subj','cmderr','perr','peakspeed','cmddistance','perfdistance','duration'};

      for i =1:max(movTab.subj)
        temptab     = movTab(movTab.subj==i,:);
        temptgt     = kinarmProcess.targetAccuracyFromMovTab(temptab,'verbose',verbose);
        inds        = find(abs(temptgt.(whichdist) - ampDesM) < ampThreshM);
        perr        = mean(temptgt.errorMovementAxis(inds),'omitnan');
        cmderr      = cmdaccuracyM;
        peakspeed   = mean(temptgt.peakspeed(inds),'omitnan');
        cmddistance = mean(temptgt.cmddistance(inds),'omitnan');
        perfdistance= mean(temptgt.perfdistance(inds),'omitnan');
        duration    = mean(temptgt.duration(inds),'omitnan');
        distTab(i,:) = {i,...
          cmderr,...
          perr,...
          peakspeed,...
          cmddistance,...
          perfdistance,...
          duration};
      end

    end

    function [projv,tanv] = projectedVelocityForTarget(c3d,inds,movtabrow)
      % function out = projectedVelocityForTarget(c3d,inds,trow)
      % use the movtabrow to project the velocity along the reach dimension. 
      % return projected velocity for a given target.
      
      r_mov     =  movtabrow.endpointScoredXY' - movtabrow.startpointScoredXY';
      r_mov_n   = r_mov./norm(r_mov);
      vmat      = [c3d.Right_Hand_XVel(inds)';c3d.Right_Hand_YVel(inds)'];
      projv     = dot(repmat(r_mov_n,1,length(vmat)),vmat);
      tanv      = (c3d.Right_Hand_XVel(inds).^2+c3d.Right_Hand_YVel(inds).^2)';
    end

    function [f] = figureBellShapesOverTimeColoredByX(blockc3d,curtab,varargin)

      twocolors = [    0.7482    0.9246    0.7456;    0.0314    0.2510    0.5059];
      orderCol  = 'distance';
      figHard   = 12000;
      figNum    = figHard;
      alpha     = 0.4;
      lw        = 1;

      for i = 1 : 2 : length(varargin)
        option = varargin{i};
        value = varargin{i + 1};
        switch option
          case 'twocolors'
            twocolors = value;
          case 'fignum'
            figNum = value;
          case 'orderby'
            orderCol = value;
          case 'alpha'
            alpha = value;
          case 'linewidth'
            lw = value;
        end
      end
      ordStack    = [curtab.(orderCol)];
      map         = multigradient(twocolors,'interp','hsv','length',length(ordStack));
      
      [~,ind]     = sort(ordStack,'ascend');
      
      if figNum == figHard
        f = figure(figHard);
      else
        f = gcf();
      end
      
      for i = 1:length(ordStack)
        indbuff    = 100;
        curMovInds  = (curtab.startiSlice(ind(i))-indbuff):curtab.endiSlice(ind(i))+indbuff;
        if ~isnan(curMovInds)
          pv = kinarmProcess.projectedVelocityForTarget(blockc3d,curMovInds,curtab(ind(i),:));
          t  = (1:length(curMovInds))*0.001 - 0.001;
          plot(t,pv,'color',[map(i,:),alpha],'linewidth',lw); hold on;
        end
      end
    end

    function [c3d,meanPosFRR] = forceRateRateFromC3D(c3d,varargin)
      % function forceRateRateFromC3D(inData,varargin)
      % compute mean force rate from a c3d
      % adds fields to c3d input:
      % Right_FRR nx2
      % Left_FRR nx2

      sr = 1000;
      cutoff = 5;
      torquefield = '_TorquesJoint';
      for i = 1 : 2 : length(varargin)
        option = varargin{i};
        value = varargin{i + 1};
        switch option
          case 'sr'
            sr = value;
          case 'cutoff'
            cutoff = value;
          case 'torquefield'
            torquefield = value
        end
      end

      % update torque field to have right in front of it.
      righttorquefield = ['Right',torquefield];
      lefttorquefield = ['Left',torquefield];

      % ensure that this field exists.
      if ~isfield(c3d,righttorquefield)
        c3d = c3d;
        meanPosFRR = [];
        fprintf('No torques available. Please run invDynFromC3D() first.\n');
        return
      end

      c3d.Right_FRR(:,1) = kinarmProcess.forceRateRate(c3d.(righttorquefield)(:,1), sr, cutoff);
      c3d.Right_FRR(:,2) = kinarmProcess.forceRateRate(c3d.(righttorquefield)(:,2), sr, cutoff);
      c3d.Left_FRR(:,1) = kinarmProcess.forceRateRate(c3d.(lefttorquefield)(:,1), sr, cutoff);
      c3d.Left_FRR(:,2) = kinarmProcess.forceRateRate(c3d.(lefttorquefield)(:,2), sr, cutoff);

      meanPosFRR = struct;
      tDur = length(c3d.Right_FRR)*1/sr;
      t = 0:1/sr:(length(c3d.Right_FRR)-1)*(1/sr);
      tOneSec = 1.0;
      maskNoRipple = t > tOneSec & t < (t(end)-tOneSec);

      % here we average positive force rate rate.
      timeNoRipple = tDur - 2*(tOneSec);
      shfr = trapz(t,(c3d.Right_FRR(:,1) .* (c3d.Right_FRR(:,1)>0) .* maskNoRipple(:)));
      elbfr = trapz(t,(c3d.Right_FRR(:,2) .* (c3d.Right_FRR(:,2)>0).* maskNoRipple(:)));
      bothfr = shfr+elbfr;
      meanPosFRR.RightMeanPosFRR = [shfr,elbfr,bothfr] ./ timeNoRipple;

      shfr = trapz(t,(c3d.Left_FRR(:,1) .* (c3d.Left_FRR(:,1)>0) .* maskNoRipple(:)));
      elbfr = trapz(t,(c3d.Left_FRR(:,2) .* (c3d.Left_FRR(:,2)>0).* maskNoRipple(:)));
      bothfr = shfr+elbfr;
      meanPosFRR.LeftMeanPosFRR = [shfr,elbfr] ./ timeNoRipple;

      % here we do average force rate, both directions, multiplying
      % negative force rate by negative 1.
      shfr = trapz(t,( abs(c3d.Right_FRR(:,1) )  .* maskNoRipple(:)));
      elbfr = trapz(t,( abs(c3d.Right_FRR(:,2) ) .* maskNoRipple(:)));
      bothfr = shfr+elbfr;
      meanPosFRR.RightMeanFRR = [shfr,elbfr,bothfr] ./ timeNoRipple;

      shfr = trapz(t, ( abs(c3d.Right_FRR(:,1) ) .* maskNoRipple(:)));
      elbfr = trapz(t,( abs(c3d.Right_FRR(:,2) ) .* maskNoRipple(:)));
      bothfr = shfr+elbfr;
      meanPosFRR.LeftMeanFRR = [shfr,elbfr] ./ timeNoRipple;

    end

    function [posPower, avgPosPower,t] = positivePower(inPower,varargin)

      sr = 1000;
      for i = 1 : 2 : length(varargin)
        option = varargin{i};
        value = varargin{i + 1};
        switch option
          case 'sr'
            sr = value;
        end
      end

      t = 0:1/sr:(1/sr*(length(inPower)-1));
      posPower = (inPower'.* (inPower'>0))';
      work = cumtrapz(t(:),posPower);
      avgPosPower = sum(work(end,:)) / t(end);
    end

    function [projHandVec,r_home2hand] = projHandOnMov(dataXY,homexy,tgtxy)
      % function out = projectedHandOnMov(dataIn,homexy,tgtxy)
      % inputs:
      % 1. Nx2 dataXY
      % 2. 2x1 or 1x2 homexy
      % 3. 2x1 or 1x2 tgtxy
      % returns:
      % 1. 1xN projHand signed scalar.
      % later, perpendicular signed scalar
      % 2xN vector home2hand]
      % assumes time is first dimension of dataIn,
      % length(dataIn) is bigger than 2 and is the length
      % of time dimension,
      % and assumes that homexy and tgtxy are 1x2 or 2x1.
      homexy = homexy(:); %column vec
      tgtxy = tgtxy(:);   %column vec
      dataT = dataXY';    %column vec
      nT = length(dataT);

      r_home2goal = tgtxy - homexy;
      r_home2handMat = dataT - homexy;
      r_home2goalMat = repmat(r_home2goal,1,nT);

      projHandVec = dot(r_home2handMat,r_home2goalMat) ...
        / norm(r_home2goal);

      r_home2projMat = projHandVec * r_home2goal ./ norm(r_home2goal);
      r_perpMat = r_home2handMat - r_home2projMat;
      %####for now we don't need the rest. but todo: return scalars and
      %vectors

    end

    %function out = handVelPlot(dataIn)
    function out = handSpeedPlot(posx,posy,sr,varargin)
      if nargin < 3 % in case we are calling from old code.
        sr = 1000;
      end

      homexy = [];
      tgtxy = [];
      for i = 1 : 2 : length(varargin)
        option = varargin{i};
        value = varargin{i + 1};
        switch option
          case 'sr'
            sr = value;
          case 'homexy'
            homexy = value;
          case 'tgtxy'
            tgtxy = value;
        end
      end
      x = kinarmProcess.lowpassSimple(posx,sr,12);
      vx = gradient(x,1/sr);
      y = kinarmProcess.lowpassSimple(posy,sr,12);
      vy = gradient(y,1/sr);

      out.tanvel = sqrt(vx.^2+vy.^2);
      out.vx = vx;
      out.vy = vy;

      if ~isempty(homexy)
        projhand = kinarmProcess.projHandOnMov([posx(:),posy(:)],...
          homexy,tgtxy);
        out.vprojhand = gradient(projhand,1/sr);
      end

    end


    function fraterate = forceRateRate(force,sr,cutoff)
      % function meanForceRateRate(tor,sr,filt)
      % HELPER function.
      % stomp on the forces, take two derivatives
      forceLP = kinarmProcess.lowpassSimple(force,sr,cutoff);
      frate = gradient(forceLP,1/sr);
      fraterate = gradient(frate,1/sr);

    end


    function out = allFromStruct(istruct,fname)
      % function out = allFromStruct(istruct,fname)
      % HELPER function.
      % grab all values from structure istruct with fieldname fname.
      % supports vector variables, organizes the return matrix as
      % subj x var.
      %
      % Note: will catch nan values for a subject even if the field was a vector and the
      % placeholding nan was a scalar.
      %
      % Note: the first subject in istruct must have a real value or there will
      % be problems because we use the first subject to set the size of the
      % return matrix.
      %
      % Note: will handle 1 level of sub-structure data. I.e. if istruct has .dat
      % and .dat has .num, you may pass ('.dat.num') as fname.

      dim1 = length(istruct);
      temp = istruct(1).(fname);
      dim2 = length(temp(:));

      out = zeros(dim1,dim2);
      for ist =1:dim1
        if ~isfield(istruct(ist),fname)
          v = zeros(1,dim2)*nan;
        else
          v = istruct(ist).(fname)(:);
        end
        out(ist,:) = v;
      end;
      out(find(out==9999))=nan;
    end
    % end;

    function ym = mean_discrete(x,y)
      xdisc = unique(x);
      ym = zeros(length(xdisc),1);
      for i = 1:length(xdisc)
        inds = find(x==xdisc(i));
        ym(i) = mean(y(inds),'omitnan');
      end
    end

    
    function [tout, pvout, pvmax] = timewarp4peakspeed(t, pv, varargin)
      % function timewarp4peakspeed(t, pv)
      % resample such that the peakspeed is aligned to the n/2'th index of the length of t. 
      % 
      verbose = 0;

      for i = 1 : 2 : length(varargin)
        option = varargin{i};
        value = varargin{i + 1};
        switch option
          case 'verbose'
            verbose = value;
        end
      end


      [pvmax,ind] = max(pv);
      N = length(t);
      Nhalf = round(N/2);

      if ind == 1 || ind == N
        tout = t;
        pvout = pv;
        fprintf('max was at 1 or end; no timewarp done.\n');
      else
        tout = [linspace(t(1),t(ind),Nhalf)';linspace(t(ind+1),t(N), N-Nhalf)'];
        pvout = interp1(t, pv, tout)';
      end
      if verbose
        figure();
        subplot(2,2,1);
        plot(t, pv,'linewidth',3);
        hold on;
        plot(tout,pvout,'.-','linewidth',1.5);
        subplot(2,2,2)
        plot(pvout);hold on;
        line([Nhalf,Nhalf],ylim());
        pause;
      end
    end

    function [xs,ys,vals] = fillmissingsurf(xs,ys,vals)
      % function vals = fillmissingsurf(xs,ys,vals)
      % Linearly interpolate xs and ys over zeros, and vals over nans.
      xs(xs==0) = nan;
      ys(ys==0) = nan;
      xs = fillmissing(xs,'linear');
      ys = fillmissing(ys,'linear');
      inn = find(~isnan(vals) & vals~=0);
      F = scatteredInterpolant(xs(inn),ys(inn),vals(inn));
      [ixn,iyn] = find(isnan(vals) | (vals==0));
      for i= 1:length(ixn)
        vals(ixn(i),iyn(i)) = F(xs(ixn(i),iyn(i)),ys(ixn(i),iyn(i)));
      end

    end
    
    function out = allfrom2DCell(thestructarray,vname)
      % function out = allfrom2Dcell(thestructarray,fname)
      % HELPER function.
      % grab all values from 2D array istruct with fieldname fname.
      % supports vector variables, organizes the return matrix as
      % subj x var.
      %
      % Note: will catch nan values for a subject even if the field was a vector and the
      % placeholding nan was a scalar.
      %
      % Note: the first subject in istruct must have a real value or there will
      % be problems because we use the first subject to set the size of the
      % return matrix.
      %
      % Note: will handle 1 level of sub-structure data. I.e. if istruct has .dat
      % and .dat has .num, you may pass ('.dat.num') as fname.

      dim1 = size(thestructarray,1);
      dim2 = size(thestructarray,2);
      out = zeros(dim1,dim2)*nan;
      for ist =1:dim1
        for jst = 1:dim2
          if isstruct(thestructarray{ist,jst})
            out(ist,jst) = thestructarray{ist,jst}.(vname);
          end
        end
      end
    end
    % end;

    %%% all_from_field == allFromStruct. Used to use this name for the function, just keeping it to not break old code.
    function out = all_from_field(istruct,fname)
      out = kinarmProcess.allFromStruct(istruct,fname);
    end

    function newdata = lowpassSimple(data,samprate,cutoff,order,direction)
      % newdata = lowpass(data,samprate,cutoff,order)
      %
      % performs a lowpass filtering of the input data
      % using an nth order zero phase lag butterworth filter
      %
      % given -> data (data)
      %	-> samprate (Hz)
      %	-> cutoff freq (Hz)
      %	-> order of filter (optional: default 2nd order)
      %
      % returns -> filtered data

      if nargin==4
        direction=2;
      end;
      if nargin==3 order=2; direction=2;end; % default to 2nd order

      % get filter paramters A and B
      [B,A] = butter(order,cutoff/(samprate/2));
      % perform filtering
      if direction==2
        newdata = filtfilt(B,A,data);
      else
        newdata = filter(B,A,data);
        %?newdata(1:50)=0;
      end
    end

    function newdata = lowpassSimpleWrap(data,samprate,cutoff,order,direction)
      % function newdata = lowpassSimpleWrap(data,samprate,cutoff,order,direction)
      for i = 1:size(data,2)
        newdata(:,i) = lowpassSimple(data(:,i),samprate,cutoff,order,direction);
      end
    end

    function [cellIndList] = movementsByLowSignedVel(inputVel, varargin)
      % default
      threshVel = 0.2;

      for i = 1 : 2 : length(varargin)
        option = varargin{i};
        value = varargin{i + 1};
        switch option
          case 'thresh'
            threshVel = value;
        end
      end

      [~,iPeaks] = kinarmProcess.getPeaksBetweenAThresholdValue(...
        inputVel,threshVel);
      [indsFullCylce,cellIndList] = kinarmProcess.getFullCycleMovements(...
        inputVel,iPeaks);
    end

    function [x, b, s, submat] = arrangeDataForSubjectStats(xs, yin,dimSub)
    % function [Amixed,b] = rotateDataForSubjectStats(xs, yin,dimSub)  
    % assumes a subject's data is in a row. 
    % two things we'd like to do here: 
    % 1- LINALG: for linear algebra mixed-effects, we arrange to have
    %    A- NxNrptmsr columns for a mixed offset
    %    B- 1  column vector X for the predictor (force rate, distance, etc)
    %    C- 1  column vector b the data we are predicting (energy, peak speed, distance)
    % 2- STATSpkg: for using matlab STATS 'fitlm' or 'fitlme', we arrange in a table(), 
    %    where we have a subj column as a random fixed effehect, 
    %    some predictor column, and some outcome. 
    % note: linalg doesn't know what to do with Nans. Stats just ignores those rows. 

      if dimSub ==2
        yin = yin';
      end
      if size(yin,1)==1
        %we only have one subject; it was in a row. 
        N = 1;
      elseif size(yin,2)==1
        %we only have one subj, all in a column.
        yin = yin'; % rotated so that subjects are in rows.
        N  = 1;
      else
        N = size(yin,1);
      end
      Nrptmsr = size(yin,2);

      %%% reshape the predictor (xs) values as a column. Allow them to be subject-specific.  
      %%% call this Xvec.
      if size(xs,1) == 1 || size(xs,2)==1 
        x = repmat(xs(:),N,1);
        
        % then we have data that are a matrix, but we're using a vector ofx. 
        % warn the user that this might be a mistake. 
        if (size(yin,1)>1 && size(yin,2)>1)
          fprintf('Note: not using individual X values in regression. \n');
        end
      else % if we have subjects differing along D1, we flip then splat.
        xsT = xs';
        x = xsT(:);
        fprintf('Note: using individual X values in regression. \n');
        fprintf('Assuming data are arranged as subj X xvals (internally, we then 1-flip and 2-(:)). \n');
      end

      %%% LINALG: make Amixed, a matrix that has nsub+1 columns, 
      %%% STATSpkg: will use (s, x .^power, b)
      b = yin';
      b = b(:);
      s = repmat((1:N)',1,Nrptmsr);
      s = s';
      s = s(:);

      submat  = zeros(N*Nrptmsr,N);
      for i = 1:N
        r = (i-1)*Nrptmsr+1:i*Nrptmsr;
        submat(r,i) = 1;
      end
    end

    function out = fitIndividualOffsetPowerLaw(xs,yin,powerLaw,xFitLine,varargin)
      % function out = fitIndividualOffsetPowerLaw(xs,yin,powerLaw,xFitLine)
      [x,b,s,submat] = kinarmProcess.arrangeDataForSubjectStats(xs, yin,1);
      xpow   = x.^powerLaw;
      Amixed  = [submat, x];
      indPow  = size(Amixed,2);
      
      %%%%%%% THERE ARE 3 parts to the following statistics:
      % 1. fixed effects (no individuual offset).
      % 2. mixed effects (manually with a matrix, and using stats toolbox).
      % 3. presubtracted subject effect followed by fixed effect.

      %%%% PART 1: FIXED EFFECTS
      %%%Fixed model effects ONLY. Does not do individual offset.
      % first, use regress to return us the values.

      Tbl = table(s,xpow,b);
      lm  = fitlm(Tbl,'b ~ xpow','Intercept',true);
      out.Fixed.Coef  = lm.Coefficients.Estimate;
      out.Fixed.lm    = lm;
      out.Fixed.stats = [lm.Rquared.Ordinary, [], lm.coefTest,[]]; %r, f, p, error var
      ind             = ~isnan(xpow+b);
      out.Fixed.CoefLINALG = [xpow(ind),ones(sum(ind),1)]\b(ind);
      
      out.Fixed.xout = xFitLine;
      out.Fixed.yout = predict(lm,xFitLine);
        
      %check the linear model coefficients using slash output. assume we want an offset.
      bstar             = out.Fixed.CoefLINALG(1)*xpow+out.Fixed.CoefLINALG(2)*ones(length(b),1);
      out.Fixed.p       = out.Fixed.stats(3);
      explainedVar      = sum((bstar-b).^2,'omitnan');
      totalVar          = sum((b - mean(b)).^2,'omitnan');
      out.Fixed.checkR2 = 1-(explainedVar/totalVar);
      %%%% END PART 1: FIXED EFFECTS fixed effects linear model

      %%%% PART 2: MIXED EFFECTS     
      opt = statset('LinearMixedModel');
      opt.Display = 'iter';

      % manually perform mixed model statistics. try to remove nans. 
      ind             = ~isnan(sum(Amixed,2) + b); 
      out.Mixed.coefs = Amixed(ind) \ b(ind);

      % now
      mdllme = fitlme(Tbl,'b ~ xpow + (1|subject)','OptimizerOptions', opt); % linear model
      out.Mixed.mdllme =mdllme;

      % Compute R^2 the same way that Surabhi and JFinley do
      sigma_f = var(mdllme.designMatrix*mdllme.fixedEffects);           % Variance of the fixed effects
      sigma_eps = var(mdllme.residuals);                                % Variance of the model residuals
      out.Mixed.R2SimhaFinley2019 = sigma_f/(sigma_f + sigma_eps);      % Marginal R-squared

      for i =1:n
        out.Mixed.labels{i}=['offset ',num2str(i)];
      end
      out.Mixed.labels{n+1} = ['k*freqs^',num2str(powerLaw)];

      %%% checking model fit and errors.
      for i =1:length(Amixed)
        isSub = find(Amixed(i,1:n)==1);
        intercp = out.Mixed.coefs(isSub);
        yhat(i) = intercp+out.Mixed.coefs(indPow)*Amixed(i,indPow);
      end

      yhat = yhat(:);
      out.Mixed.A = Amixed;
      out.Mixed.b = b;
      out.Mixed.yhat = yhat(:);
      out.Mixed.e=(b-out.Mixed.yhat).^2;
      out.Mixed.mse= (b-mean(b)).^2;
      out.Mixed.R2=1-sum(out.Mixed.e)./sum(out.Mixed.mse);


      %% compute an individual offset removed y. Good For plotting
      for i = 1:length(out.Mixed.coefs)-1
        intercp = out.Mixed.coefs(i);
        out.Mixed.yIndividualOffsetSubtracted(i,:) = yin(i,:) - intercp;
      end
      %% compute adjusted ys (scatterplot)
      colXPow=counter; %for easier typing
      for i =1:length(Amixed)
        isSub = find(Amixed(i,1:n)==1);
        intercp = out.Mixed.coefs(isSub);
        yhat(i) = intercp+out.Mixed.coefs(indPow)*Amixed(i,indPow);
        yAdjusted(i) = b(i)-intercp;
        ystarAdjusted(i) = out.Mixed.coefs(colXPow)*Amixed(i,colXPow);
      end
      %%%% /END PART 2 Mixed model.

      %% PART 3 Manual and regress()-based adjusted R2 and p (removing individual offset effect).
      % JDW 2019-10: quite confident R2 manual removal same as regress-R2
      % (great!).
      % JDW 2019-10: not sure about P. still looks too significant...
      % R2 adjusted to manually remove the effect of the individual offsets.
      % note that this number is the same as the result returned from
      % 'regress()' if you pre-subtract each subject's individual offset which is
      % done below.
      out.MixedAdjusted.e = (yAdjusted-ystarAdjusted).^2;
      out.MixedAdjusted.mse = (yAdjusted-mean(yAdjusted)).^2;
      out.MixedAdjusted.R2 = 1-sum(out.MixedAdjusted.e)./sum(out.MixedAdjusted.mse);
      % here is 'regress()' to compare the adjusted R2 values.
      % JDW 2019-10. I'm not sure what i think about the adjusted pvalue however.
      % perhaps the p-value
      [out.MixedAdjusted.Regressb,out.MixedAdjusted.Regressbint,out.MixedAdjusted.Regressr,...
        out.MixedAdjusted.Regressrint,out.MixedAdjusted.Regressstats] = regress(...
        yAdjusted(:),[Amixed(:,colXPow),ones(length(yAdjusted),1)]);
      %and below these three lines r2 f and p because i forget the ordering.
      out.MixedAdjusted.RegressR2 = out.MixedAdjusted.Regressstats(1);
      out.MixedAdjusted.RegressF = out.MixedAdjusted.Regressstats(2);
      out.MixedAdjusted.Regressp = out.MixedAdjusted.Regressstats(3);
      out.MixedAdjusted.ErrorVar = out.MixedAdjusted.Regressstats(4);
      %return fitlines if a 4th input column was passed.
      % return both a non-offset-subtracted and an offset subtracted line.
      if nargin==4
        offset = mean(out.Mixed.coefs(1:n));
        xgain = out.Mixed.coefs(n+1);
        out.Mixed.xout = xFitLine;
        out.Mixed.yout = offset + xgain*xFitLine.^powerLaw;
        out.Mixed.youtIndividualOffsetSubtracted = xgain*xFitLine.^powerLaw;
      end
    end

    % movements less than 2 mad's below mean in DUR.
    function robustZ = robustZScore(input)
      medVal      = median(input(:),1,"omitnan");
      robustZ  = abs(input - medVal) ./ mad(input,1);
    end

    function [valPeaks,indPeaks] = getPeaksBetweenAThresholdValue(inVec,threshForMovement)
      % function [valPeaks,indPeaks] = getPeaksBetweenAThresholdValue(inVec,threshForMovement)
      % given in input vector of tangential velocities,
      % check each time that a threshold is crossed (up and down), and
      % return the peak inbetween those crossings.
      % Useful for parsing out naturalistic reaches that may have some
      % bumpy speed decelerations.
      % A threshold threshForMovement
      % is used to determine the start of an event (like a reach).
      % it's a good idea to provide lowpassed data to this function.
      % author: jeremy wong 2021-06-28

      inVecZeroed =inVec - threshForMovement;
      % get zero crossings
      indsCrosses = find(inVecZeroed>0 & circshift(inVecZeroed,1)<=0);
      %clean. because circshift wraps last, sometimes get the first index.
      if indsCrosses(1)==1
        indsCrosses = indsCrosses(2:end);
      end

      [valPeaks,indPeaks]=deal(zeros(length(indsCrosses)-1,1));
      for iC = 1:length(indsCrosses)
        if iC == length(indsCrosses) %the last trial
          curRange = indsCrosses(iC):length(inVec);
        else
          curRange = indsCrosses(iC):indsCrosses(iC+1);
        end
        [valPeaks(iC),indTemp] = max(inVec(curRange));
        indPeaks(iC) = indTemp + indsCrosses(iC)-1;
      end
    end

    function [indFullCycle,outList] = getFullCycleMovements(velocities, indPeaks, verbose)
      % function out = getFullCycleMovements(ilist,vals)
      % For processing cyclic reaching movements.
      % inputs: velocities: -> Nx1 continuous SIGNED velocities over
      % time. either use handProjVel() or x or y vel.
      % inputs: indPeaks: where the peaks occur
      % verbose:
      % if a double negative or double positive is detected, we do the simple
      % thing and just remove that from the list.
      % RETURNS:
      % cell array 'outlist' of size n. you can extract movements
      % demarcated by these

      if nargin() < 3
        verbose = 0;
      end

      %%% start at a positive direction reach
      signVels = sign(velocities(indPeaks));
      positiveStart = signVels(1)==1;
      while ~positiveStart
        %snip the first one
        indPeaks = indPeaks(2:end);
        %   update signVels
        signVels = sign(velocities(indPeaks));
        positiveStart = signVels(1)==1;
      end

      %%% now that we're starting positive, get the deltaV
      delta = diff(signVels);

      % since this velocity is signed, the delta should be 2
      binaryFullCycle = delta==2 & circshift(delta,1,1) == -2;
      indFullCycle = indPeaks(binaryFullCycle);

      outList = {};
      counter = 1;
      % loop through the list of peaks.
      for indCur = 1:length(indPeaks)
        % for a cyclic movement, the next cycle should begin two
        % indices away.
        % away.
        indEnd        = indCur+2;
        if any(indPeaks(indCur)==indFullCycle) && indEnd < length(indPeaks)
          outList{counter} = indPeaks(indCur):indPeaks(indEnd);
          counter = counter+1;
        end
      end

      if verbose
        figure();
        plot(velocities,'linewidth',3);
        for i = 1:length(outList)
          plot(velocities(outList{i}),'r-.');
        end
      end
    end


    function indicesCellArray = peakToPeakVelIndices(iPeakVels,indGoodFullCycle)
      %function indicesCellArray = peakToPeakVelIndices(iPeakVels,indGoodFullCycle)
      % return peak-to-peak (full cycle) movements, intended for sinusoidal
      % motion processing.
      % INPUTS: peak velocity indices
      indicesCellArray = {};
      for indCur = 1:length(iPeakVels)
        indEnd = indCur+2;
        if any(iPeakVels(indCur)==indGoodFullCycle) && indEnd < length(iPeakVels)
          inds = iPeakVels(indCur):iPeakVels(indCur+2);
          indicesCellArray{indCur}=inds;
        end
      end
    end

    function avgPosPower = cyclicAnalysisFromC3D(c3dStack,varargin)
      vecSize = 200;
      sr = 1000;
      lowpassForSlice = 6 ;
      velThreshMPerS = 0.3;%
      addedMass = 0;%
      bodyMass = 70;
      for i = 1 : 2 : length(varargin)
        option = varargin{i};
        value = varargin{i + 1};
        switch option

          case 'ndownsample'
            vecSize = value;

          case 'velthresh'
            velThreshMPerS = value;

          case 'sr'
            sr = value;

          case 'addedmass'
            addedMass = value;

          case 'bodymass'
            bodyMass = value;
        end
      end

      c3dStack = kinarmProcess.invDynFromC3D(c3dStack,'addedMass',...
        addedMass,bodyMass,'bodyMass');
      c3dStack = kinarmProcess.forceRateRateFromC3D(c3dStack);

      x = c3dStack.Right_HandX; %slice up movements using right-h.
      y = c3dStack.Right_HandY;

      handspeedData   = kinarmProcess.handSpeedPlot(x,y,sr);
      handspeed       = handspeedData.tanvel;
      handspeed       = kinarmProcess.lowpassSimple(handspeed,sr,lowpassForSlice);
      % clean the handspeeds from 3*STD outliers.
      handspeed(handspeed>(mean(handspeed)+3*std(handspeed))) = nan;
      [~,iPeakVels] = kinarmProcess.getPeaksBetweenAThresholdValue(handspeed,velThreshMPerS);

      [~,indlist] = kinarmProcess.getFullCycleMovements(handspeedData.vx,iPeakVels);

      % for convenience. make a matrix of right and left shoulder and
      % elbow angles.

      avgPosPower =[];
      for iCur = 1:length(indlist)
        inds = indlist{iCur};
        t = 0:1/sr:1/sr*(length(inds)-1)
        duration      = (length(inds))*1/sr;
        curPowRight   = c3dStack.Right_Power(inds, :);
        curPowLeft    = c3dStack.Left_Power(inds, :);
        apr           = sum(trapz(t,(curPowRight>0).*curPowRight, 1)) / duration;
        apl           = sum(trapz(t,(curPowLeft>0).*curPowLeft, 1)) / duration;
        avgPosPower = [apr,apl,apr+apl,apr+apl/2;avgPosPower];

      end

    end

    function [tauGlobal,tauLocal,powers,meanMs,Ms] = invDynKinarmVirtualPower(...
        theQ,theQDot,theQDDot,robTor,P)
      %function [tauGlobal,tauLocal] = invDynKinarmVirtualPower(...
      %       theQ,theQDot,theQDDot,robTor,P)
      % Compute inverse dynamics
      % for added mass, need 3 new params in P: IHandMass (0?), mHandMass,
      % cHandMass (axial distance from elbow joint to added mass)
      % this uses equations of motion derived using virtual power.

      m1 = P.L1_M;
      m2 = P.L2_M;
      m3 = P.L3_M;
      m4 = P.L4_M;

      I1 = P.L1_I;
      I2 = P.L2_I;
      I3 = P.L3_I;
      I4 = P.L4_I;

      Imot1 = P.MOTOR1_I;
      Imot2 = P.MOTOR2_I;

      if isfield(P,'IHandMass')
        IHandMass = P.IHandMass;
        mHandMass = P.mHandMass;
        cHandMass = P.cHandMass;
      else
        IHandMass = 0;
        mHandMass = 0;
        cHandMass = 0;
      end

      l1 = P.L1_L;
      l3 = P.L3_L;
      cx1 = P.L1_C_AXIAL;
      ca1 = P.L1_C_ANTERIOR;
      cx2 = P.L2_C_AXIAL;
      ca2 = P.L2_C_ANTERIOR;
      cx3 = P.L3_C_AXIAL;
      ca3 = P.L3_C_ANTERIOR;
      cx4 = P.L4_C_AXIAL;
      ca4 = P.L4_C_ANTERIOR;
      Q25 = P.L2_L5_ANGLE;

      iloop =1:1:length(theQ);
      q1 = theQ(iloop,1);
      q2 = theQ(iloop,2);
      q1dot = theQDot(iloop,1);
      q2dot = theQDot(iloop,2);
      qddot1 = theQDDot(iloop,1);
      qddot2 = theQDDot(iloop,2);
      curRobTor1 = robTor(iloop,1);
      curRobTor2 = robTor(iloop,2);

      M11 = I1 + I4 + Imot1 + ca1^2*m1 + ca4^2*m4 + cx1^2*m1 + cx4^2*m4 + l1^2*m2 + l1^2*mHandMass;
      M12 = cx2*l1*m2*cos(q1 - q2) - ca4*l3*m4*sin(Q25 + q1 - q2) + cHandMass*l1*mHandMass*cos(q1 - q2) + ca2*l1*m2*sin(q1 - q2) + cx4*l3*m4*cos(Q25 + q1 - q2);
      %M21 = cx2*l1*m2*cos(q1 - q2) - ca4*l3*m4*sin(Q25 + q1 - q2) + cHandMass*l1*mHandMass*cos(q1 - q2) + ca2*l1*m2*sin(q1 - q2) + cx4*l3*m4*cos(Q25 + q1 - q2),
      M22 = mHandMass*cHandMass^2 + m2*ca2^2 + m3*ca3^2 + m2*cx2^2 + m3*cx3^2 + m4*l3^2 + I2 + I3 + IHandMass + Imot2;
      M21 = M12; %Kane/Lagrange derivations produce a symmetric mass matrix.

      meanMs(1,1) = mean(M11);
      meanMs(1,2) = mean(M12);
      meanMs(2,1) = mean(M21);
      meanMs(2,2) = mean(M22);
      Ms = [repmat(M11,length(M12),1),M12,M21,repmat(M22,length(M12),1)];

      F1 = -q2dot.^2.*(cx4*l3*m4*sin(Q25 + q1 - q2) - ca2*l1*m2*cos(q1 - q2) + ...
        cx2*l1*m2*sin(q1 - q2) + cHandMass*l1*mHandMass*sin(q1 - q2) + ca4*l3*m4*cos(Q25 + q1 - q2));
      F2 = q1dot.^2.*(cx4*l3*m4*sin(Q25 + q1 - q2) - ca2*l1*m2*cos(q1 - q2) + ...
        cx2*l1*m2*sin(q1 - q2) + cHandMass*l1*mHandMass*sin(q1 - q2) + ca4*l3*m4*cos(Q25 + q1 - q2));


      %matrix multiply manual for vectorization.
      %M ddq = F+[u1-u2;u2];
      %DDQ = [qddot1;qddot2];
      %M = [M11,M12;M21,M22];
      tausNet1 = M11 .* qddot1 + M12.*qddot2 - F1 - curRobTor1 + curRobTor2;
      tausNet2 = M21 .* qddot1 + M22.*qddot2 - F2 - curRobTor2;

      tauGlobal(iloop,1) = tausNet1 + tausNet2;
      tauGlobal(iloop,2) = tausNet2;

      tauG = tauGlobal(iloop,:)';
      tauL = kinarmProcess.tauGlob2Loc(tauG);
      tauLocal(iloop,:) = tauL';%because i've organized tau as row vecs.

      powerSho = theQDot(:,1).*tauGlobal(:,1);
      powerElb = theQDot(:,2).*tauGlobal(:,2) - ...
        theQDot(:,1).*tauGlobal(:,2);
      powers = [powerSho,powerElb];
    end

    function tauLoc = tauGlob2Loc(tauGlob)
      % function tauLoc = tauGlob2Loc(tauGlob)
      JacLoc2Glob = [1,0;1,1];
      tauLoc = JacLoc2Glob' * tauGlob;
    end

    function tauGlob = tauLoc2Glob(tauLoc)
      %function tauGlob = tauLoc2Glob(tauLoc)
      JacGlob2Loc = [1,0;-1,1];
      tauGlob = JacGlob2Loc' * tauLoc;
    end

    % here is how the kinarm hand is calculated!
    % mxy, the hand position from math.
    % hxy, the hand position that comes out. 
    function [mxy, hxy,q12,sxy] = verifyKinarmHandCalculation(c, ind)
      if nargin < 2
        ind = 1;
      end

      hxy = [c.Right_HandX(ind),c.Right_HandY(ind)];
      arm = c.CALIBRATION;
      l1 = arm.RIGHT_L1;
      l2 = arm.RIGHT_L2;
      sx = arm.RIGHT_SHO_X;
      sy = arm.RIGHT_SHO_Y;
      l2ptr = arm.RIGHT_PTR_ANTERIOR;
      
      q12 = [c.Right_L1Ang(ind),c.Right_L2Ang(ind)];
      mx = sx + l1*cos(c.Right_L1Ang(ind)) + l2*cos(c.Right_L2Ang(ind)) + ...
           l2ptr*cos(c.Right_L2Ang(ind)+pi/2);
      my = sy + l1*sin(c.Right_L1Ang(ind)) + l2*sin(c.Right_L2Ang(ind)) + ...
           l2ptr*sin(c.Right_L2Ang(ind)+pi/2);      
      mxy = [mx,my];

      sxy = [sx,sy];

    end

    function out = verify_xy2joints(hxy, self)
    % function out = verify_xy2joints(hxy, self)
      x=hxy(1);
      y=hxy(2);
      l1 = self.l1;
      l2 = self.l2;
      l4 = sqrt(self.l2^2 + self.lhandant^2);
      c = sqrt(x^2+y^2);
      gamma = atan2(y,x);
      beta = acos((l1^2+c^2-l4^2) / (2*l1*c));
      q1 = gamma - beta;
      theta = acos((l1^2+l4^2-c^2) / (2*l1*l4));
  
      sigma = acos(l2/l4);
      q2 = pi - (theta - q1 + sigma);
      out = [q1,q2];
    end

    function [P] = getKinarmSegmentParameters(c3d,whichLR,mass)
      %function [P] = getKinarmSegmentParameters(c3d,whichLR,mass)
      c3d = KINARM_add_subject_inertia(c3d,'mass',mass);
      DB_ = KINARM_create_trough_database('human_2016');
      c3d = KINARM_add_trough_inertia(c3d, 'trough_db', DB_, 'trough_size', 'estimate', 'trough_location', 'estimate');

      inertiaCombRight = KINARM_combine_inertias(c3d.RIGHT_KINARM,...
        c3d.RIGHT_KINARM_TROUGHS);
      inertiaCombRight = KINARM_combine_inertias(inertiaCombRight,...
        c3d.RIGHT_ARM);

      inertiaCombLeft = KINARM_combine_inertias(c3d.LEFT_KINARM,...
        c3d.LEFT_KINARM_TROUGHS);
      inertiaCombLeft = KINARM_combine_inertias(inertiaCombLeft,...
        c3d.LEFT_ARM);

      switch whichLR
        case 'left'
          humanP = c3d.LEFT_ARM;
          P = inertiaCombLeft;
        case 'right'
          humanP = c3d.RIGHT_ARM;
          P = inertiaCombRight;
      end
      P.L1_L            = humanP.L1_L;
      P.L2_L            = humanP.L2_L;
      P.L2_PTR_ANTERIOR = c3d.CALIBRATION.RIGHT_PTR_ANTERIOR;
      P.cHandMass       = P.L2_L;
      P.mHandMass       = 0;
      P.IHandMass       = 0;
    end

    function [kinematicsAndKinetics, inertiaRight,inertiaLeft]= inverseDynamicsKinarm(kinematics,subStruct,dofigure,skipFilt)
      % function [invDyn, kinematics]= inverseDynamicsKinarm(kinematics,subStruct,dofigure,skipFilt)
      % this uses the inverse dynamics from KINARM technologies. Note
      % that you cannot add mass to the hand with this code.
      %
      % input is:
      % kinematics kindata structure array.
      % subStruct that has mass (that's all it is at the moment).
      % optional do figure and skipFilt.

      if nargin < 3
        dofigure = 0;
      end
      for trial = 1:length(kinematics)
        % rename the kinematics since matlat is annoying and cannot handle
        % multiple indexes.
        kinData = kinematics(trial);

        % add the accelerations
        kinData = KINARM_add_hand_kinematics(kinData);

        % add subject inertia components the to data structure
        kinData = KINARM_add_subject_inertia(kinData,'mass',subStruct.mass);

        % Create a database of arm trough inertias. Note: these data are old, but
        % work as a first approximation
        % 	DB = KINARM_create_trough_database('human_2008');

        % use the new DB for troughs as of 2016.
        DB_ = KINARM_create_trough_database('human_2016');

        % add arm trough inertia components the to data structure
        kinData = KINARM_add_trough_inertia(kinData, 'trough_db', DB_, 'trough_size', 'estimate', 'trough_location', 'estimate');

        % assume friction of 0.15 ian oct 25 2017.
        % apparently we can't get in and measure friction or viscosity,
        % because unlike older Kinarms we can't take it apart.
        % we assume negligible viscosity (the 3rd parameter).
        kinData = KINARM_add_friction(kinData,0.15,0);
        % filter the data
        if nargin <4
          trialDataPostFilterDecision = c3d_filter_dblpass(kinData, 'enhanced', 'fc', 10);
        elseif skipFilt
          trialDataPostFilterDecision=kinData;
        else
          trialDataPostFilterDecision = c3d_filter_dblpass(kinData, 'enhanced', 'fc', 10);
        end

        % calculate the InterMuscular torques
        trialDataPostFilterDecision = KINARM_add_torques(trialDataPostFilterDecision);
        kinematicsAndKinetics(trial) = trialDataPostFilterDecision;
        % plotting
        if dofigure
          fnum = 3;
          figure()
          subplot(fnum,1,1)
          plot([RightShoVel, LeftShoVel ])
          ylabel('Sho Vel')

          subplot(fnum,1,2)
          plot([RightElbVel, LeftElbVel])

          ylabel('Elb vel')

          subplot(fnum,1,3)
          plot([RightShoTorIM,LeftShoTorIM])
          legend({'rightShoTor','leftShoTor'})
          ylabel('Sho Tor')
          pause;
        end

      end

      %%%return the inertias of the arms
      inertiaRight = KINARM_combine_inertias(kinData.RIGHT_KINARM,...
        kinData.RIGHT_KINARM_TROUGHS);
      inertiaRight = KINARM_combine_inertias(inertiaRight,...
        kinData.RIGHT_ARM);

      inertiaLeft = KINARM_combine_inertias(kinData.LEFT_KINARM,...
        kinData.LEFT_KINARM_TROUGHS);
      inertiaLeft = KINARM_combine_inertias(inertiaLeft,...
        kinData.LEFT_ARM);
    end

    % function out = validateInverseDynamics(stackedMovOriginal,subj)
    % return inverse dynamics computed for the right arm.
    % also return the parameters P that we use for the InvDyn computation.
    function [out,dataStruct,P] = validateInverseDynamics(stackedMovOriginal,subj)

      % get invdyn via kinarm code.
      dataStruct = kinarmProcess.inverseDynamicsKinarm(stackedMovOriginal,subj);

      % merge the arm and robot inertias.
      P = kinarmProcess.getKinarmSegmentParameters(dataStruct,'right',subj.mass);

      % for my inv dyn, i want Q, Qdot, QDDot.
      theQ = [dataStruct.Right_L1Ang,dataStruct.Right_L2Ang];
      theQDot = [dataStruct.Right_L1Vel,dataStruct.Right_L2Vel];
      theQDDot = [dataStruct.Right_L1Acc,dataStruct.Right_L2Acc];

      % extract the kinarm-computed human torques
      tauKinarm = [dataStruct.Right_SHOTorIM,dataStruct.Right_ELBTorIM];

      % extract the cmded robot torques
      torquesRobot = [dataStruct.Right_M1TorCMD,dataStruct.Right_M2TorCMD];
      friction = [dataStruct.Right_M1TorFRC,dataStruct.Right_M1TorFRC];

      % compute inverse dynamics using virtual power.
      [tauGlobal,tauLocal] = kinarmProcess.invDynKinarmVirtualPower(theQ,theQDot,theQDDot,torquesRobot,friction,P);

      %
      plot(tauGlobal,'linewidth',4); hold on;
      plot(tauKinarm,'linewidth',2);

      plot(tauGlobal-tauKinarm);
      legend({'jer-globalS','jer-globalE','kinarm-ShoTorIM','kinarm-ElbTorIM','errorS','errorE'})
      % what this shows is that the kinarm-shotorIM is the global torque (not the local torque),
      % since Jer's VP method was done in external reference frame.
      t = 0:0.001:0.001*(length(theQDot)-1);
      out.torquesExternalVP = tauGlobal;
      out.torquesLocalVP = tauLocal;
      out.torquesExternalBKIN = tauKinarm;
      out.t = t;
    end

    function [movStack,iIndividualMovements]=returnStackedMovements(kinC3D,subStruct,igrp,tDelta,DO_FIG1)
      % function [movStack,iIndividualMovements]=returnStackedMovements(kinC3D,subStruct,igrp,tDelta,DO_FIG1)
      % inputs:
      %     kinC3D -> single element kinarm data structure.
      %     subStruct -> needs to have a field 'hasLongEMG'
      %     igrp -> vector of desired movements.
      %     tDelta -> optional time between recordings (for Tyler's Kinarm, the
      %     correct value as of 2017 was 0.003 s).
      %     DO_FIG1 -> print a figure to see hand positions [XY] over Time.
      % outputs:
      %     movStack -> same datastructure, but just continuous.
      %     iIndividualMovements -> the indices of the movement slices.
      if nargin<4
        tDelta = 0.003;
        disp('Assuming constant tDelta of 0.003 between movements.');
      end
      if subStruct.hasLongEMG
        fields={'Left_M1TorCMD','Left_M2TorCMD','Left_RecTorque_M1','Left_RecTorque_M2','Right_M1TorCMD','Right_M2TorCMD','Right_RecTorque_M1','Right_RecTorque_M2','Left_L1Ang','Left_L2Ang','Left_L1Vel','Left_L2Vel','Left_L1Acc','Left_L2Acc','Right_L1Ang','Right_L2Ang','Right_L1Vel','Right_L2Vel','Right_L1Acc','Right_L2Acc','Right_HandX','Right_HandY','Left_HandX','Left_HandY','ACH0','ACH1','ACH2','ACH3','ACH4','ACH5','ACH6','ACH7','ACH8','ACH9','ACH10','ACH11','ACH12','ACH14','ACH14','ACH15','ACH16','ACH17'};
        fieldsRenameFrom={'ACH16','ACH17'};
      else
        fields={'Left_M1TorCMD','Left_M2TorCMD','Left_RecTorque_M1','Left_RecTorque_M2','Right_M1TorCMD','Right_M2TorCMD','Right_RecTorque_M1','Right_RecTorque_M2','Left_L1Ang','Left_L2Ang','Left_L1Vel','Left_L2Vel','Left_L1Acc','Left_L2Acc','Right_L1Ang','Right_L2Ang','Right_L1Vel','Right_L2Vel','Right_L1Acc','Right_L2Acc','Right_HandX','Right_HandY','Left_HandX','Left_HandY','ACH0','ACH1','ACH2','ACH3','ACH4','ACH5','ACH6','ACH7','ACH8'};
        fieldsRenameFrom={'ACH4','ACH5'};
      end
      fieldsRenameTo={'Hands_X','Hands_Y'};
      % loopstyle: initialize and build upon.
      %initialize
      movStack = kinC3D(igrp(1));
      tAll =.000:0.001:0.001*(length(movStack.Right_HandX)-1);
      tAll = tAll(:);
      tOff = tAll(end)+tDelta;
      lOff = length(tAll);
      tIndividualMovements(1) = tOff;
      lenIndividualMovements(1) = length(tAll)+2;
      for i =2:length(igrp)
        kinTemp = kinC3D(igrp(i));
        tCurMov = 0.000:0.001:0.001*(length(kinTemp.Right_HandX)-1);
        for jField = 1:length(fields)
          movStack.(fields{jField}) = [movStack.(fields{jField});kinTemp.(fields{jField})];
        end

        tAll = [tAll;tCurMov(:)+tOff];
        tOff = tAll(end)+tDelta;
        tIndividualMovements(i) = tOff;
        lOff = lOff + length(tCurMov);
        lenIndividualMovements(i) = length(tCurMov)+2;
      end

      for i=1:length(fieldsRenameFrom)
        movStack.(fieldsRenameTo{i}) = movStack.(fieldsRenameFrom{i});
      end
      movStack.t = tAll;
      iIndividualMovements = cumsum(lenIndividualMovements);

      %interpolate those stupid 3ms.
      tProper = 0:0.001:movStack.t(end);
      for jField=1:length(fields)
        if length(movStack.t)==length(movStack.(fields{jField}))
          temp = interp1(movStack.t,movStack.(fields{jField}),tProper');
          movStack.(fields{jField}) = temp(:);
        else
          fprintf(['skipping ,',fields{jField}]);
        end
      end
      movStack.t = tProper(:);
      iIndividualMovements(end)=length(movStack.Right_HandX); %don't need added delta for last.
      DO_FIG1 = 0;
      if DO_FIG1
        f1 = 999;
        tOffsetS=0;

        figure(f1);hold on;
        plot(movStack.Right_HandX,movStack.Right_HandY);
        set(gca,'ColorOrderIndex',1);
        plot(movStack.Right_HandX,movStack.Right_HandY);
        set(gca,'ColorOrderIndex',1);

        f2=998;
        figure(f2);

        nSamp = length(movStack.Right_HandX);
        tplot = 0.001:0.001:(nSamp*0.001);
        tplot = tplot+tOffsetS;

        subplot(3,1,1);
        plot(tplot,movStack.Right_HandX); hold on;
        set(gca,'ColorOrderIndex',1);
        plot(tplot,movStack.Right_HandY);
        set(gca,'ColorOrderIndex',1);
        plot(tplot,movStack.Left_HandX);
        set(gca,'ColorOrderIndex',1);
        plot(tplot,movStack.Left_HandY);
        set(gca,'ColorOrderIndex',v1);

        subplot(3,1,2);
        pOff = 10;
        hold on;
        tOffsetS = tplot(end);
      end

    end
    % compute the equations of motion of the kinarm.
    % local oordinate system. s
     % function out = deriveEOM()
    function out = deriveEOM()
      syms m1 m2 m3 m4 I1 I2 I3 I4 l1 l3 cx1 ca1 cx2 ca2 cx3 ca3 cx4 ca4 Q25 g Imot1 Imot2 cHandMass mHandMass IHandMass real
      syms q1 q1dot q2 q2dot real
      %% DEFINE COMS
      % 20200226: EOM for kinarm with added mass
      % these EOMs include motor rotational inertias and endpoint masses.
      XYS = [cx1*cos(q1) - ca1*sin(q1);
        cx1*sin(q1) + ca1*cos(q1);
        l1*cos(q1) + cx2*cos(q2) - ca2*sin(q2);
        l1*sin(q1) + cx2*sin(q2) + ca2*cos(q2);
        cx3*cos(q2 - Q25) - ca3*sin(q2 - Q25);
        cx3*sin(q2 - Q25) + ca3*cos(q2 - Q25);
        l3*cos(q2 - Q25) + cx4*cos(q1) - ca4*sin(q1);
        l3*sin(q2 - Q25) + cx4*sin(q1) + ca4*cos(q1);
        0;
        0;
        0;
        0
        l1*cos(q1) + cHandMass*cos(q2)
        l1*sin(q1) + cHandMass*sin(q2)];

      % PHI_BODY_INERTIALFRAME: what is the orientation of the body in inertial reference frame?
      PHI_BODY_INERTIALFRAME = [q1;q2;q2;q1;q1;q2;q2];
      % what are the masses associated with each body?
      m=[m1;m2;m3;m4;0;0;mHandMass];
      % what are the inertias associated with each body?
      I=[I1 I2 I3 I4 Imot1 Imot2 IHandMass];
      % what are the DOF of the system?
      q = [q1;q2];
      % what are the derivatives of the DOFs (2D is straightforward)
      qdot=[q1dot;q2dot];

      n_bodies = length(m);
      n_q = length(q);
      n_dim = 2;

      % LOOP.
      M = zeros(n_q,n_q);
      F = zeros(n_q,1);
      G = zeros(n_q,1);

      for i =1:n_bodies
        XY_cur = XYS((i-1)*n_dim+1:(i)*n_dim,:);
        PHI_cur = PHI_BODY_INERTIALFRAME(i);
        JF = jacobian(XY_cur,q);
        fprintf('Jacobian %i\n',i);
        disp(JF);
        JR = jacobian(PHI_cur,q);
        sigma = jacobian(JF*qdot, q)*qdot;
        sigma_R = jacobian(JR*qdot,q)*qdot;
        meye = eye(n_dim,n_dim)*m(i);
        M = M + JF' * meye * JF + ...
          JR' * I(i) * JR;
        M = simplify(expand(M));
        F = F - ( JF' * meye * sigma + JR' * I(i) * sigma_R); %minus is Remy convention.

        %GRAVITY
        % rotation matix: in case we want to reach in gravity...
        gam = 0;
        A_IB = [[cos(gam);sin(gam)],[-sin(gam);cos(gam)]];
        FG = m(i)*-g*A_IB'*[0;1];
        G = G + JF'*FG;
        %/GRAVITY
      end
      F=simplify(expand(F));
      G=simplify(expand(G));
      out = struct;
      out.F = F;
      out.M = M;
      out.G = G;
    end

    function[out,JLout,JRout,XYS] = deriveLocalEOM()
      syms m1 m2 m3 m4 I1 I2 I3 I4 l1 l3 cx1 ca1 cx2 ca2 cx3 ca3 cx4 ca4 Q25 g Imot1 Imot2 cHandMass mHandMass IHandMass real
      syms q1 q1dot q2 q2dot real
      %% DEFINE COMS
      % 20200226: EOM for kinarm with added mass
      % these EOMs include motor rotational inertias and endpoint masses.
      XYS = [cx1*cos(q1) - ca1*sin(q1);
        cx1*sin(q1) + ca1*cos(q1);
        l1*cos(q1) + cx2*cos(q1+q2) - ca2*sin(q1+q2);
        l1*sin(q1) + cx2*sin(q1+q2) + ca2*cos(q1+q2);
        cx3*cos(q1+q2 - Q25) - ca3*sin(q1+q2 - Q25);
        cx3*sin(q1+q2 - Q25) + ca3*cos(q1+q2 - Q25);
        l3*cos(q1+q2 - Q25) + cx4*cos(q1) - ca4*sin(q1);
        l3*sin(q1+q2 - Q25) + cx4*sin(q1) + ca4*cos(q1);
        0;
        0;
        0;
        0;
        l1*cos(q1) + cHandMass*cos(q1+q2);
        l1*sin(q1) + cHandMass*sin(q1+q2)];

      % PHI_BODY_INERTIALFRAME: what is the orientation of the body in inertial reference frame?
      PHI_BODY_INERTIALFRAME = [q1;q1+q2;q1+q2;q1;q1;q1+q2;q1+q2];
      % what are the masses associated with each body?
      m=[m1;m2;m3;m4;0;0;mHandMass];
      % what are the inertias associated with each body?
      I=[I1 I2 I3 I4 Imot1 Imot2 IHandMass];
      % what are the DOF of the system?
      q = [q1;q2];
      % what are the derivatives of the DOFs (2D is straightforward)
      qdot=[q1dot;q2dot];

      n_bodies = length(m);
      n_q = length(q);
      n_dim = 2;

      % LOOP.
      M = zeros(n_q,n_q);
      F = zeros(n_q,1);
      G = zeros(n_q,1);

      JLout = [];
      JRout = [];
      for i =1:n_bodies
        XY_cur = XYS((i-1)*n_dim+1:(i)*n_dim,:);
        PHI_cur = PHI_BODY_INERTIALFRAME(i);
        JL = jacobian(XY_cur,q);
        JLout(i).val = JL;
        fprintf('Jacobian %i\n',i);
        disp(JL);
        JR = jacobian(PHI_cur,q);
        JRout(i).val = JR;

        sigma = jacobian(JL*qdot, q)*qdot;
        sigma_R = jacobian(JR*qdot,q)*qdot;
        meye = eye(n_dim,n_dim)*m(i);
        M = M + JL' * meye * JL + ...
          JR' * I(i) * JR;
        M = simplify(expand(M));
        F = F - ( JL' * meye * sigma + JR' * I(i) * sigma_R); %minus is Remy convention.

        %GRAVITY
        % rotation matix: in case we want to reach in gravity...
        gam = 0;
        A_IB = [[cos(gam);sin(gam)],[-sin(gam);cos(gam)]];
        FG = m(i)*-g*A_IB'*[0;1];
        G = G + JL'*FG;
        %/GRAVITY
      end
      F=simplify(expand(F));
      G=simplify(expand(G));
      out = struct;
      out.F = F;
      out.M = M;
      out.G = G;
    end

    function [q,qd,qdd,tauR]=convertGlob2Loc(theQ,theQDot,theQDDot,robGlobTor)

      % handle dimensions.
      columnVecOut = 0;%in general, time is down dimension 1 (column).
      if size(theQ,1)>size(theQ,2)
        %then rotate to prepare for jacobian multiplication.
        theQ = theQ';
        theQDot = theQDot';
        theQDDot = theQDDot';
        robGlobTor = robGlobTor';
      else
        columnVecOut = 1;
      end

      jacGlob2Loc = [1,0;-1,1];
      q = jacGlob2Loc * theQ;
      qd = jacGlob2Loc * theQDot;
      qdd = jacGlob2Loc * theQDDot;
      tauR = kinarmProcess.tauGlob2Loc(robGlobTor);

      if columnVecOut
      else
        q = q';
        qd = qd';
        qdd = qdd';
        tauR = tauR';
      end

    end

    function [tauLocal,tauGlobal,powers,meanMs,Ms,theQDot] = invDynKinarmJointVirtualPower(...
        theQ,theQDot,theQDDot,robTor,P)
      %function [tauGlobal,tauLocal] = invDynKinarmJointVirtualPower(...
      %       theQ,theQDot,theQDDot,robTor,P)
      % Compute inverse dynamics
      % for added mass, need 3 new params in P: IHandMass (0?), mHandMass,
      % cHandMass (axial distance from elbow joint to added mass)
      % this uses equations of motion derived using virtual power.
      % note:
      % here we assume the input robot torques are global torques.
      % this is because this is the coordinate system used in the
      % kinarm code. we call 'convertGlob2Loc'
      m1 = P.L1_M;
      m2 = P.L2_M;
      m3 = P.L3_M;
      m4 = P.L4_M;

      I1 = P.L1_I;
      I2 = P.L2_I;
      I3 = P.L3_I;
      I4 = P.L4_I;

      Imot1 = P.MOTOR1_I;
      Imot2 = P.MOTOR2_I;

      if isfield(P,'IHandMass')
        IHandMass = P.IHandMass;
        mHandMass = P.mHandMass;
        cHandMass = P.cHandMass;
      else
        IHandMass = 0;
        mHandMass = 0;
        cHandMass = 0;
      end

      l1 = P.L1_L;
      l3 = P.L3_L;
      cx1 = P.L1_C_AXIAL;
      ca1 = P.L1_C_ANTERIOR;
      cx2 = P.L2_C_AXIAL;
      ca2 = P.L2_C_ANTERIOR;
      cx3 = P.L3_C_AXIAL;
      ca3 = P.L3_C_ANTERIOR;
      cx4 = P.L4_C_AXIAL;
      ca4 = P.L4_C_ANTERIOR;
      Q25 = P.L2_L5_ANGLE;

      [theQ,theQDot,theQDDot,robTor]=kinarmProcess.convertGlob2Loc(...
        theQ,theQDot,theQDDot,robTor);


      iloop =1:1:length(theQ);
      q1 = theQ(iloop,1);
      q2 = theQ(iloop,2);
      q1dot = theQDot(iloop,1);
      q2dot = theQDot(iloop,2);
      qddot1 = theQDDot(iloop,1);
      qddot2 = theQDDot(iloop,2);
      curRobTor1 = robTor(iloop,1);
      curRobTor2 = robTor(iloop,2);

      % derived local
      M11 = I1 + I2 + I3 + I4 + IHandMass + Imot1 + Imot2 + ca1^2*m1 + ca2^2*m2 + ca3^2*m3 + ca4^2*m4 + cx1^2*m1 + cx2^2*m2 + cx3^2*m3 + cx4^2*m4 + cHandMass^2*mHandMass + l1^2*m2 + l3^2*m4 + l1^2*mHandMass + 2*cx2*l1*m2*cos(q2) + 2*cHandMass*l1*mHandMass*cos(q2) + 2*cx4*l3*m4*cos(Q25 - q2) - 2*ca2*l1*m2*sin(q2) - 2*ca4*l3*m4*sin(Q25 - q2);
      M12 = I2 + I3 + IHandMass + Imot2 + ca2^2*m2 + ca3^2*m3 + cx2^2*m2 + cx3^2*m3 + cHandMass^2*mHandMass + l3^2*m4 + cx2*l1*m2*cos(q2) + cHandMass*l1*mHandMass*cos(q2) + cx4*l3*m4*cos(Q25 - q2) - ca2*l1*m2*sin(q2) - ca4*l3*m4*sin(Q25 - q2);
      M21 = M12;%symmetric mass matrix
      M22 = mHandMass*cHandMass^2 + m2*ca2^2 + m3*ca3^2 + m2*cx2^2 + m3*cx3^2 + m4*l3^2 + I2 + I3 + IHandMass + Imot2;

      meanMs(1,1) = mean(M11);
      meanMs(1,2) = mean(M12);
      meanMs(2,1) = mean(M21);
      meanMs(2,2) = mean(M22);
      Ms = [M11,M12,M21,repmat(M22,length(M12),1)];

      F1=  q2dot.*(2*q1dot + q2dot).*(ca2*l1*m2*cos(q2) - ca4*l3*m4*cos(Q25 - q2) + cx2*l1*m2*sin(q2) + cHandMass*l1*mHandMass*sin(q2) - cx4*l3*m4*sin(Q25 - q2));
      F2 = -q1dot.^2.*(ca2*l1*m2*cos(q2) - ca4*l3*m4*cos(Q25 - q2) + cx2*l1*m2*sin(q2) + cHandMass*l1*mHandMass*sin(q2) - cx4*l3*m4*sin(Q25 - q2));

      %matrix multiply manual for vectorization.
      %M ddq = F+[u1-u2;u2];
      %DDQ = [qddot1;qddot2];
      %M = [M11,M12;M21,M22];
      tausNet1 = M11 .* qddot1 + M12.*qddot2 - F1 - curRobTor1 + curRobTor2;
      tausNet2 = M21 .* qddot1 + M22.*qddot2 - F2 - curRobTor2;

      tauLocal(iloop,1) = tausNet1 + tausNet2;
      tauLocal(iloop,2) = tausNet2;

      tauG = kinarmProcess.tauLoc2Glob(tauLocal');
      tauGlobal(iloop,1) = tauG(1,:)';
      tauGlobal(iloop,2) = tauG(2,:)';

      powerSho = theQDot(:,1).*tauGlobal(:,1);
      powerElb = theQDot(:,2).*tauGlobal(:,2) - ...
        theQDot(:,1).*tauGlobal(:,2);
      powers = [powerSho,powerElb];
    end

    function [out] = naturalFrequency(dataC3D,fs,welchMax,welchWindow,welchOverlap)
      if nargin < 3
        fs = 1000;
        welchMax = 5;
        welchWindow = 10000;
        welchOverlap = 1000;
      end

      colPickWindow='Right_L1Ang';
      xPlot = dataC3D.(colPickWindow);
      plot(xPlot);

      fprintf('pick window');
      xmouse = ginput(2);
      xmouse = round(xmouse(:,1));
      %%
      % pwelch%%
      figure;
      colData='Right_L1Vel';
      x = dataC3D.(colData)(xmouse(1):xmouse(2));
      x = x-mean(x);
      tPickWindow = 0:0.001:(length(x)-1)*1/1000;
      xPickWindow = dataC3D.(colPickWindow)(xmouse(1):xmouse(2));
      figure;
      subplot(4,1,1);
      plot(tPickWindow,xPickWindow);
      grid on;

      subplot(4,1,2);
      plot(tPickWindow,x);

      %pwelch use:
      % x,windowlength,windowoverlap,freqs,sampfreq
      [pxx,fW] = pwelch(x,welchWindow,welchOverlap,0:.2:5,fs);
      %       [pxx,fW] = pwelch(x,10000);
      %       fW = fW*1000;
      powerWelch = 10*log10(pxx);
      %get the max.
      [~,ind]=max(powerWelch)
      maxFreq = fW(ind);

      % plot ther esults
      subplot(4,1,3);
      plot(fW,powerWelch)
      xlabel('Frequency (Hz)')
      ylabel('PSD (dB/Hz)')
      xlim([0,welchMax]);
      title(sprintf('max freq PWelch %.2f Hz', maxFreq'));

      % do fft.
      Y = fft(x,length(x));
      Pyy = Y.*conj(Y)/(length(x));
      f = 1000/length(x)*(0:(length(x)/2));

      [~,ind] = max(Pyy);
      maxFreqFFT = f(ind);
      subplot(4,1,4);
      plot(f,Pyy(1:(length(x)/2)+1))
      title(sprintf('Power spectral density max %.2f Hz',maxFreqFFT));
      xlabel('Frequency (Hz)')
      xlim([0,welchMax])

      subplot(4,1,1)
      ylabel('ext shoulder angle')
      xlabel('time (s)')
      subplot(4,1,2);
      ylabel('shoulder vel')
      xlabel('time (s)')

      out.fW = fW;
      out.fWelch = maxFreq;
      out.powerWelch = powerWelch;

    end
  end
end