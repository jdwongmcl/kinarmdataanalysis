[audioStop,fs] = audioread('middlec.wav');
[audioStart,fs] = audioread('a4.wav');
audioStop = audioStop(1:(fs/4));
audioStart = audioStart(1:(fs/4));
sound(audioStop,fs);
sound(audioStart,fs);
audioStart = audioStart;

%%
fA = 440;
omegaA = 2*pi*fA;

fC = 300;
omegaC = 2*pi*fC;


t = linspace(0,0.5,fs/4);
audioStart = sin(omegaA*t);
audioStop  = sin(omegaC*t);

% Increase the pitch by 3 semitones and listen to the result.


durMovs = [60/116];
%%
fprintf('Start moving on the high sound, and stop moving on the low sound!\n');
numEachPractice = 20;
for i = 1:-0.2:.6
  durStops = [60/116]*i;
  count=1;
  while count < numEachPractice
    sound(audioStop,fs);
    pause(durStops);
    sound(audioStart,fs);
    pause(durMovs);
    count = count+1;
  end
  
end