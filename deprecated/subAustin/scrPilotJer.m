fname = '201912  .xlsx';
%% experiment 1: stop/start
time_start = 4.25;
tmin_ends = [time_start+3,  time_start+6+2,  time_start+6+5+2,  time_start+6+5+5+2,  time_start+6+5+5+5+2];
damping = [0,0,0,0];
CONT=1;STOP_START = 2;
condition = [CONT,STOP_START,STOP_START,CONT];
[out,bbb]=readK5(fname);
met = metabolicsBreathByBreath(bbb,tmin_ends,mass);
plot(met.t,met.wattsO2CO2all)
