%% need to change 

fname = 'YXU_2020-02-06_09-25-49.zip'
metMinOffset = 4.1;
subStruct = kinarmProcess('Jer',75);
fnameMet = '20200206 jeremy wong (CPET Mixing Chamber)_20200206104949.xlsx';

%%
data = zip_load(fname);
data = sort_trials(data,'execution');


%% read the data's torques

loadIndex = data.c3d(1).TP_TABLE.Load(data.c3d(1).TP_TABLE.Load~=0);
viscosities = data.c3d(1).LOAD_TABLE.X_Viscosity;

%get the load number in a stupid loop
for iT = 1:length(data.c3d)
  TP(iT) = data.c3d(iT).TRIAL.TP;
  loadNumber(iT) = loadIndex(TP(iT));
  viscosity(iT) = viscosities(loadNumber(iT));
end

%% loop through the different load conditions.

clear pow;
%% compute inverse dynamics for all of the reaching movements
tic
for i =1:max(loadNumber)
  %inds is all of the movement numbers that have load 'i'.
  %viscosity(i) gives the viscosity for that loadNumber. 
  sr = 1000;
  inds = find(loadNumber==i);
  inds = inds(inds<length(data.c3d))
  curMov = kinarmProcess.returnStackedMovements(data.c3d,subStruct,inds)
  durBlock(i) = length(curMov.t)*(1/sr);
  
  iLast180s = (length(curMov.t)-180*1000):length(curMov.t);
  
%   [returnData,invDyn] = kinarmProcess.validateInverseDynamics(curMov,subStruct);
  invDyn = kinarmProcess.inverseDynamicsKinarm(curMov,subStruct)
  invDynAllBlocks(i) = invDyn;
  tempPower = kinarmProcess.mechanicalPower(invDyn,curMov);
  pow(i,1:3) = [tempPower.meanPowR,tempPower.meanPowL,tempPower.meanPowLR];
  
  clear returnData invDyn
end
toc
%%
for i =1:max(loadNumber)
  damp(i) = viscosities(i);
end

%%

subplot(2,1,1);
plot(abs(damp),pow(:,3),'bo','markersize',10);  
xlabel('damping coefficient (N/(m/s))');
ylabel('power (W)');
axis([0,20,0,6])
%%  process the metabolics
[out,bbb]=readK5(fnameMet);
minEnds = cumsum(durBlock)./60+metMinOffset;
met = metabolicsBreathByBreath(bbb,minEnds,subStruct.mass);
%%
subplot(2,1,2);
plot(abs(damp),met.wattsO2CO2,'ro','markersize',10);

%% take a look at some of the torques from inverse dynamics.
figure;
ibl = [1:5]
for i =ibl
  indBack = 15000;
  subplot(3,1,1);
  plot(invDynAllBlocks(i).Right_L1Ang(end-indBack:end),'linewidth',2);hold on;
  subplot(3,1,2);
  plot(invDynAllBlocks(i).Right_L1Vel(end-indBack:end),'linewidth',2);hold on;
  subplot(3,1,3);
  plot(invDynAllBlocks(i).Right_SHOTorIM(end-indBack:end),'linewidth',2);hold on;

%   plot(invDynAllBlocks(i).Right_L1Ang(1:10000),'linewidth',2);hold on;
%   subplot(3,1,2);
%   plot(invDynAllBlocks(i).Right_L1Vel(1:10000),'linewidth',2);hold on;
%   subplot(3,1,3);
%   plot(invDynAllBlocks(i).Right_SHOTorIM(1:10000),'linewidth',2);hold on;

end
legend(num2str(damp(ibl)'));
