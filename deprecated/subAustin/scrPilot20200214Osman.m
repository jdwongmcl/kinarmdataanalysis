%% need to cxhange only this code block for each subject. check every line!
% notes: add to path the matlab directory (and subdirectories) with
% addpath(genpath(pwd())) inside the matlab directory in dropbox folder.
% cd into the data directory manually so that you're in the subject's
% specific folder. probabaly a smarter way to do this by sticking to a
% naming convention

% experiment 1: damping
fname = 'YXU_2020-02-14_16-32-45.zip'
metMinOffset = 3.67;
name = 'Osman';
mass = 90;
subStruct = kinarmProcess('Osman',90,0);
fnameMet = '20200214 jeremy wong (CPET Mixing Chamber)_20200214173511.xlsx';
%
% experiment 2: replication
bpmReplication = [82,47];
fsReplication = bpmReplication./60
minStarts = [36,42];
fnameReplication = {'YXU_2020-02-14_17-01-55.zip'
                    'YXU_2020-02-14_17-09-32.zip'};

% experiment 3: duty factor

% experiment 4: damping
fnameDampingPilot1 = 'YXU_2020-02-14_17-33-14.zip';
fnameDampingPilot2 = 'YXU_2020-02-14_17-31-32.zip';

DATADIRNAME = AustinProject.myDataDirectory('Austin'); % austin set with his name. 

%% 
cd(DATADIRNAME);
if subStruct.doneZipLoad
  load(subStruct.name,'dampingWorkCostData');
else
  data = zip_load(fname);
  data = sort_trials(data,'execution');
end
%% compute inverse dynamics for all of the reaching movements
[powTable,invDynAll,metPow]=MechanicalWork.mechanicalAndMetabolicPower(data.c3d,...
  fnameMet,metMinOffset, name,mass);
if ~subStruct.doneZipLoad
  save([DATADIRNAME,'/processed/',subStruct.name,'dampingWorkCostData'],'curMov','powTable','invDynAll','metPow');
end


%% replication met data
[out,bbb]=readK5(fnameMet);
minEnds = minStarts+5;
met = metabolicsBreathByBreath(bbb,minEnds,subStruct.mass);

figure;plot(fsReplication,met.wattsO2CO2,'bo');

%% 
damp = zip_load(fnameDampingPilot2);
underDamp = zip_load(fnameDampingPilot1);
underDamp = sort_trials(underDamp,'execution');
damp = sort_trials(damp,'execution');
sub = kinarmProcess(name,mass);
MechanicalWork.lookAtDamping(damp,underDamp,sub);