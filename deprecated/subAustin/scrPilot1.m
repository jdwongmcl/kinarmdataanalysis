% function out = read_cosmed_csv(fname,tmin_ends,mass)
fname = '20191122 Austin Ladouceur (CPET Breath by Breath)_20191122152847.csv';
tmin_ends = [5,11,20,26]; %just an estimate for austins first dataset.
fname = '20191125 Austin Ladouceur (CPET Breath by Breath)_20191125132849.csv';
tmin_ends = [8,13,18,23,28];
mass = 75;
fname = '20191127 Austin Ladouceur (CPET Breath by Breath)_20191127134546.xlsx';
%% experiment 1: stop/start
tmin_ends = [8.25,15,20.75,28];
damping = [0,0,0,0];
CONT=1;STOP_START = 2;
condition = [CONT,STOP_START,STOP_START,CONT];
[out,bbb]=readK5(fname);
met = metabolicsBreathByBreath(bbb,tmin_ends,mass);
plot(met.t,met.wattsO2CO2all)

%% experiment 2: Frate calibration.
tmin_ends = [];
fs = [0.58,1.37,0.97];
tMinEndsExpt2=[42,52.5,65];
[out,bbb]=readK5(fname);

metExpt2 = metabolicsBreathByBreath(bbb,tMinEndsExpt2,mass);
figure;subplot(2,1,1);
plot(metExpt2.wattsO2CO2);
subplot(2,1,2);
plot(fs,metExpt2.o2means,'bo','markersize',20);
%%
tmin_ends = [];
tMinEndsExpt3=[60,66,72,77,83];
damping = [5,0,10,15,7.5]*-1;
metExpt3 = metabolicsBreathByBreath(bbb,tMinEndsExpt3,mass);
figure;
plot(damping,metExpt3.wattsM2O2CO2,'bo','markersize',20);
%% 
xlabel('damping');
ylabel('Metabolic rate (W)');


%%
data_in = dlmread(fname);

%%
t = data_in(:,4);
o2_mlpermin = data_in(:,9);
co2_mlpermin = data_in(:,10);
RQ = data_in(:,11);
bbb.t = t;
bbb.o2mlPmin = o2_mlpermin;
bbb.co2mlPmin = co2_mlpermin;
bbb.RQ = RQ;






out = metabolicsBreathByBreath(bbb,tmin_ends,mass);
damping = [-5,0,-10,-2.5,-7.5];


%% sanity check work done during the stop starts. take a look at sinusoids. just look at the movements torque power. everything 
% sanity check work done during the stop starts. take a look at sinusoids. just look at the movements torque power. everything 