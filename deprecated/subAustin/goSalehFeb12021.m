%% need to cxhange only this code block for each subject. check every line!
% notes: add to path the matlab directory (and subdirectories) with
% addpath(genpath(pwd())) inside the matlab directory in dropbox folder.
% cd into the data directory manually so that you're in the subject's
% specific folder. probabaly a smarter way to do this by sticking to a
% naming convention

% experiment 1: damping
% fname = 'YXU_2020-02-11_11-51-50.zip';
metMinOffset = 22;
name = 'Saleh';
mass = 80;
subStruct = kinarmProcess('Saleh',70,0);
fnameMet = '20210201 saleh hosseini (CPET Mixing Chamber)_20210201135555.xlsx';

startEndBPMD0C1 = [22, 28, 100, 0;
                   28, 24, 140, 0;
                   34, 40,  60, 0;
                   46, 52, 100, 1
                   53, 59, 120, 1
                   60, 66,  60, 1
                   66,112,  90, 1
                   72, 78,  90, 0
                   93, 99,   0, -99];
minEndsContinousANDSS = startEndBPMD0C1(:,2);
[dataMet,bbb] = readK5(fnameMet);
metCD = metabolicsBreathByBreath(bbb,minEndsContinousANDSS,subStruct.mass,1);


%%
figure;
indsDisc = startEndBPMD0C1(:,4)==0;
indsCont = startEndBPMD0C1(:,4)==1;
figure;plot(startEndBPMD0C1(indsDisc,3), metCD.wattsO2(indsDisc),'rx');
hold on;
plot(startEndBPMD0C1(indsCont,3), metCD.wattsO2(indsCont),'bo');

legend({'discrete','continuous'})
xlabel('frequency')
ylabel('watts')
%% saleh's mechanical power trial
fname = 'YXU_2021-02-01_12-36-54.zip';
tempData = zip_load(fname);
tempData = sort_trials(tempData,'execution');
%%
mp = MechanicalWork.inverseDynamicsAndPowerFromFile(tempData.c3d,mass);

%% look at damping to stop
% YXU_2021-03-08_15-50-15 jer
% dataUnordered = zip_load('YXU_2021-02-01_13-46-25.zip');
dataUnordered = zip_load('YXU_2021-03-08_15-50-15');
%%
figure;
data = sort_trials(dataUnordered,'execution');
for i =1:length(data.c3d)
  figure(11);
  plot(data.c3d(i).Right_HandX);hold on;
  veldat = kinarmProcess.handSpeedPlot(data.c3d(i).Right_HandX,data.c3d(i).Right_HandY,sr);
  figure(10);
  hold on;
  plot(veldat.tanvel);
end
% plot the hand speeds. 
%%
subSaleh = kinarmProcess('saleh','80');
dataS = kinarmProcess.returnStackedMovements(data.c3d,subSaleh,[1:length(data.c3d)],0.003);
sr = 1000;
veldat = kinarmProcess.handSpeedPlot(dataS.Right_HandX,dataS.Right_HandY,sr);
%
figure;

t = 0:1/sr:(length(veldat.tanvel)-1)*1/sr;
plot(t,veldat.tanvel)
% toc;
% figure(998);
% savefig([DATADIRNAME,'/processed/',subStruct.name,'dampingINVDYN']);
% figure(999);
% savefig([DATADIRNAME,'/processed/',subStruct.name,'dampingPowerANDMet']);
% 
% %% Experiment 2: mechanical power replication data. 
% % mechanical power
% for iF = 1:length(fnameReplication)
%     tempData = zip_load(fnameReplication{iF});
%     tempData = sort_trials(tempData,'execution');
%     dataC3D = tempData.c3d;
%     mechpowerReplication(iF) = MechanicalWork.inverseDynamicsAndPowerFromFile(dataC3D,mass);
%     workRateAllReplication(iF) = mechpowerReplication(iF).meanPowLR;
% end
% % metabolic power
% [out,bbb]=readK5(fnameMet);
% metReplication = metabolicsBreathByBreath(bbb,minEndsReplication,subStruct.mass);
% % add the low metabolic power from the damping experiment 1, which had a different
% % frequency. 
% ind0 = find(damp==-0.5);
% workRateAllReplication(3) = powTable(ind0,3);
% metRateAllReplication = metReplication.wattsO2CO2;
% metRateAllReplication(end+1) = metPowDamping.wattsO2CO2(ind0);
% fsReplication = [fsReplication(:);58/60];
% figure;plot(fsReplication,metRateAllReplication,'bo');
% xlabel('Frequency (s^{-1})'); ylabel('metabolic power (W)');
% savefig([DATADIRNAME,'/processed/',subStruct.name,'replication']);
% %% Experiment 3: mechanical power continuous/SS
% % mechanical power
% for iF = 1:length(fnameContinuousANDSS)
%     tempData = zip_load(fnameContinuousANDSS{iF});
%     tempData = sort_trials(tempData,'execution');
%     dataC3D = tempData.c3d;
%     mechpowerContinuousANDSS(iF) = MechanicalWork.inverseDynamicsAndPowerFromFile(dataC3D,mass);
%     workRateAllContinuousANDSS(iF) = mechpowerContinuousANDSS(iF).meanPowLR;
% end
% % metabolic power
% metContinuousANDSS = metabolicsBreathByBreath(bbb,minEndsContinousANDSS,subStruct.mass);
% 
% % add in the zero (near zero; 0.5 damping) from damping trials 
% % damping will be continuous; the other two should
% % be a variation of stop-start. 
% ind0 = find(damp==-0.5);
% workRateAllContinuousANDSS(3) = powTable(ind0,3);
% metRateAllContinuousANDSS = metContinuousANDSS.wattsO2CO2;
% metRateAllContinuousANDSS(end+1) = metPowDamping.wattsO2CO2(ind0);
% figure;plot(workRateAllContinuousANDSS,metRateAllContinuousANDSS,'bo');
% savefig([DATADIRNAME,'/processed/',subStruct.name,'continuousANDSS']);
% % here we miiight expect to see the workrate be higher for stop start. 
% % we'll need to add in force rate post-hoc, since it's not ready, and maybe
% % force-rate ends up being a better predictor than work rate. 
% 
% %% store all of the minute information!
% timeEndsAll = struct;
% timeEndsAll.minOffsetDamping = metMinOffset;
% timeEndsAll.minEndsReplication = minEndsReplication;
% timeEndsAll.minEndsContinuousANDSS = minEndsContinousANDSS;
% %% experiment 4 piloting. see what people do with a damping field.
% % probably this is just pure piloting and we need to tighten the protocol.
% dampFiles = zip_load(fnameDampingPilot2);
% underDampFiles = zip_load(fnameDampingPilot1);
% underDampFiles = sort_trials(underDampFiles,'execution');
% dampFiles = sort_trials(dampFiles,'execution');
% MechanicalWork.lookAtDamping(dampFiles,underDampFiles,subStruct);
% savefig([DATADIRNAME,'/processed/',subStruct.name,'lookatdamping']);
% 
% %% THIS TAKES A LONG TIME. about 2GB upload. That's a lot. 
% % Check your internet upload speeds and decide!
% % 5 minutes on campus internet. 
% % Probably longer at home.
% % if this isn't doable Austin, lemme know and i'll bring you an external
% % hard drive to save these processed files and figures.
% tic;
% if ~subStruct.doneZipLoad
%   save([DATADIRNAME,'/processed/',subStruct.name,'dampingWorkCostData'],...
%   'powTable','invDynAll','metPowDamping','damp',... %Experiment 1
%   'metContinuousANDSS','mechpowerContinuousANDSS','metRateAllContinuousANDSS',...%Experiment 2
%   'workRateAllContinuousANDSS',... 
%   'metReplication','mechpowerReplication','workRateAllReplication',... %Experiment 3
%   'metRateAllReplication',...
%   'dampFiles','underDampFiles','timeEndsAll'); %Experiment 4
% end
% durUpload=toc;
% fprintf(sprintf('Upload took %i seconds.\n',durUpload));
