%% need to cxhange 

% experiment 1: damping
fname = 'YXU_2020-02-06_09-25-49.zip'
metMinOffset = 2;
name = 'Ash';
mass = 60;
subStruct = kinarmProcess('Ash',60,0);
fnameMet = '20200206 jeremy wong (CPET Mixing Chamber)_20200206104949.xlsx';
%%
% experiment 2: replication
bpmReplication = [82,47];
fsReplication = bpmReplication./60
minStarts = [36,42];
fnameReplication = {'YXU_2020-02-06_09-59-55.zip'
                    'YXU_2020-02-06_10-05-59.zip'};

% experiment 3: duty factor

% experiment 4: damping
fnameDampingPilot1 = 'YXU_2020-02-06_10-44-24.zip';
fnameDampingPilot2 = 'YXU_2020-02-06_10-45-45.zip';

DATADIRNAME = AustinProject.myDataDirectory('Jer'); % austin set with his name. 

%% 
if subStruct.doneZipLoad
  load(subStruct.name,'dampingWorkCostData');
else
  
  data = zip_load(fname);
  data = sort_trials(data,'execution');
end
%% compute inverse dynamics for all of the reaching movements
[powTable,invDynAll,metPow]=MechanicalWork.mechanicalAndMetabolicPower(data.c3d,...
  fnameMet,metMinOffset, name,mass);
if ~subStruct.doneZipLoad
  save([DATADIRNAME,'/processed/',subStruct.name,'dampingWorkCostData'],'curMov','powTable','invDynAll','metPow');
end


%% replication met data
[out,bbb]=readK5(fnameMet);
minEnds = minStarts+5;
met = metabolicsBreathByBreath(bbb,minEnds,subStruct.mass);

figure;plot(fsReplication,met.wattsO2CO2,'bo');

%% 

damp = zip_load(fnameDampingPilot2);
underDamp = zip_load(fnameDampingPilot1);
underDamp = sort_trials(underDamp,'execution');
damp = sort_trials(damp,'execution');
sub = kinarmProcess(name,mass);
MechanicalWork.lookAtDamping(damp,underDamp,sub);