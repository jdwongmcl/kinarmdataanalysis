damp = zip_load('YXU_2020-01-28_11-31-06.zip');
underDamp = zip_load('YXU_2020-01-28_11-32-45.zip');
underDamp = sort_trials(underDamp,'execution');
damp = sort_trials(damp,'execution');
ry = kinarmProcess('ryan',75);



%% investigate damping vs non-damped behavior. 

F_KIN = 100;
figure(F_KIN)
sDamp = kinarmProcess.returnStackedMovements(damp.c3d,ry,1:length(damp.c3d));

vhDamp = sqrt(sDamp.Right_HandX.^2+sDamp.Right_HandY.^2);
torDamp = kinarmProcess.inverseDynamicsKinarm(sDamp,ry);
mpDamp = kinarmProcess.mechanicalPower(torDamp);
%%
sUnderDamp = kinarmProcess.returnStackedMovements(underDamp.c3d,ry,1:length(damp.c3d));
vhUnderDamp = sqrt(sUnderDamp.Right_HandX.^2+sUnderDamp.Right_HandY.^2);
torUnderDamp = kinarmProcess.inverseDynamicsKinarm(sUnderDamp,ry);
mpUnderDamp = kinarmProcess.mechanicalPower(torUnderDamp);
%%
figure;
plot(sDamp.Right_HandX,sDamp.Right_HandY);
hold on;
plot(sUnderDamp.Right_HandX,sUnderDamp.Right_HandY);
axis equal;
xlabel('hand x (m)');
ylabel('hand y (m)');
%%
figure(F_KIN);
n = 5;
subplot(n,1,1);
plot(sDamp.Right_L1Ang);
ylabel('phi sho (rad)');
subplot(n,1,2);

plot(sDamp.Right_L1Vel);
ylabel('dphidt sho (rad/s)');
subplot(n,1,3)

plot(torDamp.Right_SHOTorIM);
ylabel('tor sho (N?m)');

grid on;
%
subplot(n,1,1);hold on;
plot(sUnderDamp.Right_L1Ang);
subplot(n,1,2);hold on;
plot(sUnderDamp.Right_L1Vel);
subplot(n,1,3);hold on;
plot(torUnderDamp.Right_SHOTorIM);

subplot(n,1,4);
vhDamp = kinarmProcess.handSpeedPlot(sDamp.Right_HandX,sDamp.Right_HandY);
vhUnderDamp = kinarmProcess.handSpeedPlot(sUnderDamp.Right_HandX,sUnderDamp.Right_HandY);
plot(vhDamp);hold on;plot(vhUnderDamp);
ylabel('hand speed (m/s)');grid on;

subplot(n,1,5);
plot(mpDamp.powerOverTimeR);
hold on;
plot(mpUnderDamp.powerOverTimeR);
grid on;
%
xlabel('time (ms)');