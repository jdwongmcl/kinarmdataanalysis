fname = 'YXU_2020-01-28_10-02-27.zip'
metMinOffset = 4.1;
fnameMet1 = '20200128 ryan schroeder (CPET Mixing Chamber)_20200128110725.xlsx';
fnameMet2 = '20200128 ryan schroeder (CPET Mixing Chamber)_20200128113434.xlsx';

data = zip_load(fname);
data = sort_trials(data,'execution');
name = 'Rya';
mass = 80;

%% read the data's torques
[mechPow,metPow]=MechanicalWork.mechanicalAndMetabolicPower(dataC3d,...
  fnameMet,metMinOffset, name,mass);