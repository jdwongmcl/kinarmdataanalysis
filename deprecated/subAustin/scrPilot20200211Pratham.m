%% need to cxhange only this code block for each subject. check every line!
% notes: add to path the matlab directory (and subdirectories) with
% addpath(genpath(pwd())) inside the matlab directory in dropbox folder.
% cd into the data directory manually so that you're in the subject's
% specific folder. probabaly a smarter way to do this by sticking to a
% naming convention

% experiment 1: damping
fname = 'YXU_2020-02-11_11-51-50.zip';
metMinOffset = 3.67;
name = 'Pratham';
mass = 80;
subStruct = kinarmProcess('Pratham',80,0);
fnameMet = '20200211 jeremy wong (CPET Mixing Chamber)_20200211131336.xlsx';
%
% experiment 2: replication
bpmReplication = [82,47];
fsReplication = bpmReplication./60;
minStartsReplication = [36,42];
minEndsReplication = minStartsReplication + 5;
fnameReplication = {'YXU_2020-02-11_12-26-06.zip'
                    'YXU_2020-02-11_12-32-14.zip'};

% experiment 3: duty factor
minStartsContinuousANDSS = [50,59.16];
minEndsContinousANDSS = minStartsContinuousANDSS+5; %continuous, and the duty factor. 
fnameContinuousANDSS={'YXU_2020-02-11_12-38-22.zip'
                      'YXU_2020-02-11_12-47-18.zip'};

% experiment 4: damping
fnameDampingPilot1 = 'YXU_2020-02-11_12-53-49.zip';
fnameDampingPilot2 = 'YXU_2020-02-11_12-55-19.zip';

DATADIRNAME = AustinProject.myDataDirectory('Jer'); % austin set with his name. 
addpath(genpath(DATADIRNAME));
%% Experiment 1: mechanical power and metabolics together
cd(DATADIRNAME);
if subStruct.doneZipLoad
  load(subStruct.name,'dampingWorkCostData');
else
  data = zip_load(fname);
  data = sort_trials(data,'execution');
end

% (since the times for each damping condition are extracted from the
% kinarm movement data, we do them together)
%%
tic;
[powTable,invDynAll,metPowDamping,damp]=MechanicalWork.mechanicalAndMetabolicPower(data.c3d,...
  fnameMet,metMinOffset, name,mass);
toc;
figure(998);
savefig([DATADIRNAME,'/processed/',subStruct.name,'dampingINVDYN']);
figure(999);
savefig([DATADIRNAME,'/processed/',subStruct.name,'dampingPowerANDMet']);

%% Experiment 2: mechanical power replication data. 
% mechanical power
for iF = 1:length(fnameReplication)
    tempData = zip_load(fnameReplication{iF});
    tempData = sort_trials(tempData,'execution');
    dataC3D = tempData.c3d;
    mechpowerReplication(iF) = MechanicalWork.inverseDynamicsAndPowerFromFile(dataC3D,mass);
    workRateAllReplication(iF) = mechpowerReplication(iF).meanPowLR;
end
% metabolic power
[out,bbb]=readK5(fnameMet);
metReplication = metabolicsBreathByBreath(bbb,minEndsReplication,subStruct.mass);
% add the low metabolic power from the damping experiment 1, which had a different
% frequency. 
ind0 = find(damp==-0.5);
workRateAllReplication(3) = powTable(ind0,3);
metRateAllReplication = metReplication.wattsO2CO2;
metRateAllReplication(end+1) = metPowDamping.wattsO2CO2(ind0);
fsReplication = [fsReplication(:);58/60];
figure;plot(fsReplication,metRateAllReplication,'bo');
xlabel('Frequency (s^{-1})'); ylabel('metabolic power (W)');
savefig([DATADIRNAME,'/processed/',subStruct.name,'replication']);
%% Experiment 3: mechanical power continuous/SS
% mechanical power
for iF = 1:length(fnameContinuousANDSS)
    tempData = zip_load(fnameContinuousANDSS{iF});
    tempData = sort_trials(tempData,'execution');
    dataC3D = tempData.c3d;
    mechpowerContinuousANDSS(iF) = MechanicalWork.inverseDynamicsAndPowerFromFile(dataC3D,mass);
    workRateAllContinuousANDSS(iF) = mechpowerContinuousANDSS(iF).meanPowLR;
end
% metabolic power
metContinuousANDSS = metabolicsBreathByBreath(bbb,minEndsContinousANDSS,subStruct.mass);

% add in the zero (near zero; 0.5 damping) from damping trials 
% damping will be continuous; the other two should
% be a variation of stop-start. 
ind0 = find(damp==-0.5);
workRateAllContinuousANDSS(3) = powTable(ind0,3);
metRateAllContinuousANDSS = metContinuousANDSS.wattsO2CO2;
metRateAllContinuousANDSS(end+1) = metPowDamping.wattsO2CO2(ind0);
figure;plot(workRateAllContinuousANDSS,metRateAllContinuousANDSS,'bo');
savefig([DATADIRNAME,'/processed/',subStruct.name,'continuousANDSS']);
% here we miiight expect to see the workrate be higher for stop start. 
% we'll need to add in force rate post-hoc, since it's not ready, and maybe
% force-rate ends up being a better predictor than work rate. 

%% store all of the minute information!
timeEndsAll = struct;
timeEndsAll.minOffsetDamping = metMinOffset;
timeEndsAll.minEndsReplication = minEndsReplication;
timeEndsAll.minEndsContinuousANDSS = minEndsContinousANDSS;
%% experiment 4 piloting. see what people do with a damping field.
% probably this is just pure piloting and we need to tighten the protocol.
dampFiles = zip_load(fnameDampingPilot2);
underDampFiles = zip_load(fnameDampingPilot1);
underDampFiles = sort_trials(underDampFiles,'execution');
dampFiles = sort_trials(dampFiles,'execution');
MechanicalWork.lookAtDamping(dampFiles,underDampFiles,subStruct);
savefig([DATADIRNAME,'/processed/',subStruct.name,'lookatdamping']);

%% THIS TAKES A LONG TIME. about 2GB upload. That's a lot. 
% Check your internet upload speeds and decide!
% 5 minutes on campus internet. 
% Probably longer at home.
% if this isn't doable Austin, lemme know and i'll bring you an external
% hard drive to save these processed files and figures.
tic;
if ~subStruct.doneZipLoad
  save([DATADIRNAME,'/processed/',subStruct.name,'dampingWorkCostData'],...
  'powTable','invDynAll','metPowDamping','damp',... %Experiment 1
  'metContinuousANDSS','mechpowerContinuousANDSS','metRateAllContinuousANDSS',...%Experiment 2
  'workRateAllContinuousANDSS',... 
  'metReplication','mechpowerReplication','workRateAllReplication',... %Experiment 3
  'metRateAllReplication',...
  'dampFiles','underDampFiles','timeEndsAll'); %Experiment 4
end
durUpload=toc;
fprintf(sprintf('Upload took %i seconds.\n',durUpload));
