mass = 75;
fname = '20191127 Austin Ladouceur (CPET Breath by Breath)_20191127134546.xlsx';
%% experiment 1: stop/start
tmin_ends = [8.25,15,20.75,28];
damping = [0,0,0,0];
CONT=1;STOP_START = 2;
condition = [CONT,STOP_START,STOP_START,CONT];
[out,bbb]=readK5(fname);
met = metabolicsBreathByBreath(bbb,tmin_ends,mass);
plot(met.t,met.wattsO2CO2all)
%% task 1. what do the inverse dynamics look like for the stop/start/continuous.
% remember. we should be plotting average positive mechanical power for a
% a matched time of movements.
fname = {'YXU_2019-11-27_12-20-23.zip',
  'YXU_2019-11-27_12-28-13.zip',
  'YXU_2019-11-27_12-28-13.zip',
  'YXU_2019-11-27_12-39-51.zip'};
windows = [361,720;
  151,300;
  451,600;
  361,720];


fnameLast = '';
tic;
for ifile = 1:length(fname)
  % load the file.
  fnameCur = fname{ifile};
  if ~isequal(fnameCur,fnameLast)
    data = zip_load(fnameCur);
  end
  % set the subject parameter mass.
  subj = kinarmProcess('aus',mass,1.85);
  movementRange = windows(ifile,1):windows(ifile,2);
  
  % get stack.
  kinStacked = kinarmProcess.returnStackedMovements(data.c3d,subj,movementRange);
  
  % do invDyn.
  invDyn=kinarmProcess.inverseDynamicsKinarm(kinStacked,subj,0,0);
  % validate inv dyn by comparing my own code in external reference frame. 
  [invDynValidate,armProperties] = kinarmProcess.validateInverseDynamics(kinStacked,subj);
  power(ifile) = kinarmProcess.mechanicalPower(invDyn,kinStacked); 
  fnameLast = fnameCur;
end
toc;


%% task 2. what do the powers look like for the different damping conditions?
% for ifile = 1:length
%   % load the set.
%     data = zip_load(fnameCur);
%   end
%   % set the subject parameter mass.
%   subj = kinarmProcess('aus',mass,1.85);
%   movementRange = windows(ifile,1):windows(ifile,2);
%   
%   % get stack.
%   kinStacked = kinarmProcess.returnStackedMovements(data.c3d,subj,movementRange);
%   
%   % do invDyn.
%   invDyn=kinarmProcess.inverseDynamicsKinarm(kinStacked,subj,0,0);
%   % validate inv dyn by comparing my own code in external reference frame. 
%   invDynValidate = kinarmProcess.validateInverseDynamics(kinStacked,subj);
%   power(ifile) = kinarmProcess.mechanicalPower(invDyn,kinStacked); 
%   fnameLast = fnameCur;
% end


%% task 3. what is the resonant frequency? what is the stiffness of the shoulder tendon according to what model?
fNameNF = 'YXU_2019-11-27_13-43-18.zip';
data = zip_load(fNameNF);
output = kinarmProcess.naturalFrequency(data.c3d(1));
nfreq = output.fWelch;%the natural frequency of the arm is output.fWelch. 
% what is the stiffness of the shoulder tendon?
% assuming spring mass model, nf = sqrt(k/m)
% m is the moment of inertia about the shoulder joint. 
% what is the moment of inertia of the robot arm about the joint? 
% parallel axis theorem:
% I_aboutA = I_aboutB * L_AB^2

%% links 1 (incl. upper arm) and 3 are attached to sho. link 2_5 (incl. lower arm). link4 is attached to 3. 
% we assume full extension of the arm outward. 
  p = armProperties;
  r_l1_to_sho = norm([p.L1_C_AXIAL,p.L1_C_ANTERIOR]);
  r_l3_to_sho = norm([p.L3_C_AXIAL,p.L3_C_ANTERIOR]);
  r_l2_to_sho = norm([p.L1_L,0] + [p.L2_C_AXIAL,p.L2_C_ANTERIOR]);
  r_l4_to_sho = norm([p.L3_L,0] + [p.L4_C_AXIAL,p.L4_C_ANTERIOR]);
  
  IwholeArm_aboutSho = r_l1_to_sho^2*p.L1_M + ...
        r_l2_to_sho^2*p.L2_M + ...
        r_l3_to_sho^2*p.L3_M + ...
        r_l4_to_sho^2*p.L4_M + ...
        p.L1_I + p.L2_I + p.L3_I + p.L4_I;
  
kShoulderTendonNMPerRad = (nfreq)^2*IwholeArm_aboutSho; % this should be in nm/rad
      % the units work out...

%% task 4. how much energy could be stored in this tendon?