classdef AustinProject < handle
  properties (Access = public)
  end
  methods
    function obj=MechanicalWork()
    end
  end
  methods(Static)            % ------STATIC------ %
    function out=myDataDirectory(inName)
      if isequal(inName,'jer') ||isequal(inName,'Jer') || isequal(inName,'Jeremy') ||isequal(inName,'jeremy')  
        out = '/Volumes/kuogroup/Group members/Austin Ladouceur';
      elseif isequal(inName,'austin') || isequal(inName,'Austin')
        out = '\\136.159.130.70\students\KuoGroup\Group members\Austin Ladouceur';
      end
    end
  end
end
