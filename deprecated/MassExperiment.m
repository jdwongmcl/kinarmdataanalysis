classdef MassExperiment < handle
  properties (Access = public)
    bodyMass = [];
    handMassDistanceMeters = [];  % tape measure estimate of mass location.
    fileMet = '';
    fileKinarm = '';
    subjectName = '';             % prefix for saving processed data.
    tMinEnds = [];                % when each condition
    % finished according to cosmed clock.
    addedMass = [];               % hand mass
    freqHz =    [];               % frequency in Hz
    indCondMass = [];             % which blocks were mass
    indCondFP = [];               % which blocks were fixed power
    indCondRest = [];             % usually 8
    blockEnds = [];               % Billy prefers to write down block. there are 2 movements per block.
    haveProcessedKinarm = 0;
  end
  methods
    
    function obj=MassExperiment()
    end
  end
  
  
  methods(Static)            % ------STATIC------ %
    % This single file is meant to contain the main functions used to
    % analyze a particular experiment.
    
    function [dataWithFR,exptCondData] = analyzePowerFR(massExptSubject)
      [~,bbb]=readK5AndInferSource(massExptSubject.fileMet);
      metCosts = metabolicsBreathByBreath(bbb,massExptSubject.tMinEnds);
      
      %% preprocess the kinarm data. 10 min Billy's computer.
      
      if ~haveProcessedKinarm
        kinarmProcess.loadZipSortSave(massExptSubject.fileKinarm,'savename',[massExptSubject.subjectName,'MassAndFixedPower']);
        
      else
        processed = load([massExptSubject.subjectName,'MassAndFixedPower.mat']);
        data = processed.data; %this is because i save the processed data as the variable 'data'.
      end
      %%
      
      %% loop through movement conditions and collect power, FR data.
      
      % for ease of reading...
      freqHz = massExptSubject.freqHz;
      addedMass = massExptSubject.addedMass;
      endMovementConditions = massExptSubject.blockEnds*2;
      
      for i = 1:7 %for each block:
        sr = 1000;
        
        % 1. Stack the movements.the number of movements we analyze depends on the frequency.
        threeMinMoves = round(3*60/(1/freqHz(i)));
        movements = endMovementConditions(i)-threeMinMoves:endMovementConditions(i);
        movements = movements(10:end-10);%shave off the first and last two movements.
        dataNew = kinarmProcess.stackedKinematicsAndTorques(data.c3d,...
          movements,'both');
        
        % 2. compute mechanical power and mean pos power.
        [dataWithPower,meanMs] = kinarmProcess.invDynFromC3D(dataNew,...
          'bodymass',bodyMass,'addedmass',addedMass(i));
        mechPower.Right = dataWithPower.Right_Power;
        mechPower.Left =  dataWithPower.Left_Power;
        [posPowerR,meanPosPowerR,t]=kinarmProcess.positivePower(mechPower.Right);
        [posPowerL,meanPosPowerL]=kinarmProcess.positivePower(mechPower.Left);
        mechPowers(i,1:2)=[meanPosPowerR,meanPosPowerL];
        mechPowers(i,3)=sum(mechPowers(i,1:2));
        
        % 3. get the effective mass including human arm, robot, and added mass.
        % this is the top left corner of the mass matrix in the equations of
        % motion, returned from invDynFromC3D()
        M11(i,:) = [meanMs.Right(1,1),meanMs.Left(1,1),(meanMs.Right(1,1) + meanMs.Left(1,1))];
        
        % 4. compute force rate.
        [dataWithFR(i),meanFR]=kinarmProcess.forceRateRateFromC3D(dataWithPower);
        meanFRs(i,1:2) = [sum(meanFR.RightMeanPosFRR),sum(meanFR.LeftMeanPosFRR)];
        meanFRs(i,3) = sum(meanFRs(i,1:2));
        meanFRsShoElb(i,1:4) = [meanFR.RightMeanPosFRR,meanFR.LeftMeanPosFRR];
        ts(i) = t(end);
      end
      %/ outputs from this block: datawithFR (mov data); mechPowers; meanFRs
      
      %% Resting cost.
      restingMetWatts = metCosts.wattsO2(massExptSubject.indCondRest);
      
      %% Summary plots: mechanical power, force rate, metabolic cost.
      effectiveMass = M11(:,1)+M11(:,2);
      costMassWatts = metCosts.wattsO2(indCondMass) - restingMetWatts;
      costFP = metCosts.wattsO2(indCondFP) - restingMetWatts;
      
      plottingDetails = {'.','markersize',20};
      
      figure();
      subplot(2,2,1);
      plot(effectiveMass(indCondMass),costMassWatts,plottingDetails{:})
      title('Metabolic cost afo effective mass (M11)');
      xlabel('Effective mass (kg)');
      ylabel('Net metabolic power (W)');
      
      subplot(2,2,2);
      plot(effectiveMass(indCondMass),mechPowers(indCondMass),plottingDetails{:});
      title('mass vs avg pos mech power');
      xlabel('Effective mass (kg)');
      ylabel('Mechanical power (W)');
      
      subplot(2,2,3);
      plot(effectiveMass(indCondMass),meanFRs(indCondMass,3),plottingDetails{:});
      title('effective mass vs mean force rate');
      xlabel('Effective mass (kg)');
      ylabel('force rate');
      
      % plot
      subplot(2,2,4);
      
      plot(freqHz(indCondFP),costFP,'.','markersize',20);
      hold on; plot(freqHz(indCondFP),mechPowers(indCondFP,3)*4.8,plottingDetails{:});
      
      title('Fixed pos power');
      xlabel('Frequency (Hz)');
      ylabel('Net metabolic power (W)');
      legend({'net cost','-120%, 25% efficiency'},'location','east');
    end
  end
end