function graph_vcodes( data )

    all_ack_deltas = [];
    for n=1:length(data.c3d)
        ack_times = data.c3d(n).VIDEO_LATENCY.ACK_TIMES;
        all_ack_deltas = [all_ack_deltas, diff(ack_times) ];
    end

%     for n=2:length(all_ack_deltas) - 1
%         all_ack_deltas(n) = (all_ack_deltas(n - 1) + all_ack_deltas(n) + all_ack_deltas(n + 1)) / 3;
%     end
    
    plot(all_ack_deltas);
end

